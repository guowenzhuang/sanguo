package com.example.sanguogameserver.skill;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.RandomUtil;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillDamageAbst;

import java.util.ArrayList;
import java.util.List;

/**
 * 随机攻击全军一名武将(包括我方和敌方,等全部部队)
 */
public class RandomAttackSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "RandomAttack";
    }

    @Override
    public List<BattleTreeInfoColumn> attackRange(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, Skill skill) {
        List<BattleTreeInfoColumn> treeGenerals = new ArrayList<>();
        for (BattleTreeInfoRow tree : trees) {
            for (BattleTreeInfoColumn treeInfoColumn : tree.getTreeInfoColumns()) {
                if (treeInfoColumn.getPlayerTreeGeneral() != null) {
                    treeGenerals.add(treeInfoColumn);
                }
            }
        }
        BattleTreeInfoColumn treeGeneral = RandomUtil.randomEle(treeGenerals);
        return CollUtil.newArrayList(treeGeneral);
    }
}
