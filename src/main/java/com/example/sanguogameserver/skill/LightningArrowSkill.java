package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillDamageAbst;

import java.util.List;

/**
 * 雷电剑
 * 雷电法术,造成伤害,并有几率削弱对方防御
 */
public class LightningArrowSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "LightningArrow";
    }

    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        List<CastSkillRespDTO> castSkillRespDTOs = super.castSkill(castTree, targetTree, trees, treeSkill);
        // 造成伤害后,降低对方防御率成功几率
        String successRateStr = skill.getGailValue().split(",")[1];

        // 成功
        if (calculationCastSuccessRate(Convert.toFloat(successRateStr))) {
            // 减少对方多少防御力
            String subDefenseRateStr = skill.getGailValue().split(",")[2];
            float defense = targetTree.getPlayerTreeGeneral().getCurrentDefense() * Convert.toFloat(subDefenseRateStr);
            targetTree.getPlayerTreeGeneral().setCurrentDefense(targetTree.getPlayerTreeGeneral().getCurrentDefense() - Math.round(defense));
            castSkillRespDTOs.add(CastSkillRespDTO.success(targetTree, defense));
        } else {
            castSkillRespDTOs.add(CastSkillRespDTO.fail(targetTree));
        }
        return castSkillRespDTOs;
    }
}
