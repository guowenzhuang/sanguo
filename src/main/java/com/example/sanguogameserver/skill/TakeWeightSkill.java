package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 减少对方行动力1,敌方最少行动力1
 */
public class TakeWeightSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "TakeWeight";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getMoveCount();
        Integer add = Convert.toInt(skill.getGailValue());
        targetPlayerTreeGeneral.setMoveCount(current - add);
        if (targetPlayerTreeGeneral.getMoveCount() <= 0) {
            targetPlayerTreeGeneral.setMoveCount(1);
            return 0;
        }
        return add;
    }


}
