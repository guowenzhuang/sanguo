package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.skill.base.SkillDamageAbst;

/**
 * 冰锥术
 * 冰系法术,造成单体伤害
 */
public class IceConeSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "IceCone";
    }



}
