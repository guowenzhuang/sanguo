package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 增加防御
 */
public class SturdySkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "Sturdy";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getDefense();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setDefense(current + Math.round(add));
        return add;
    }


}
