package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.GeneralDeBuffer;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillDamageAbst;

import java.util.List;

/**
 * 火烧连营
 * 火系法术,造成少量伤害,下一回合会灼烧附近敌人
 */
public class BurningCompanyCampSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "BurningCompanyCamp";
    }

    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        List<CastSkillRespDTO> castSkillRespDTO = super.castSkill(castTree, targetTree, trees, treeSkill);
        GeneralDeBuffer generalDeBuffer=new GeneralDeBuffer();
        generalDeBuffer.setGeneralStatusType(GeneralStatusType.BurningNearby);
        generalDeBuffer.setCount(1);
        targetTree.getPlayerTreeGeneral().getBuffer().add(generalDeBuffer);
        return castSkillRespDTO;
    }
}
