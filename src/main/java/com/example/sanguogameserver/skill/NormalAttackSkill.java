package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillInterface;

import java.util.List;

/**
 * 普通攻击
 */
public class NormalAttackSkill implements SkillInterface {

    @Override
    public String getSkillCode() {
        return "NormalAttackSkill";
    }

    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        TreeGeneral playerTreeGeneral = castTree.getPlayerTreeGeneral();
        // 判断是否击中


        if (!calculationCastSuccessRate(1 - (float) targetTree.getPlayerTreeGeneral().getCurrentDodge() / 10f)) {
            return List.of(CastSkillRespDTO.fail(targetTree));
        }

        TreeGeneral beAttackTreeGeneral = targetTree.getPlayerTreeGeneral();
        int damage = attackDamage(playerTreeGeneral, beAttackTreeGeneral);
        damage = Math.max(damage, 1);
        beAttackTreeGeneral.setCurrentHealth(beAttackTreeGeneral.getCurrentHealth() - damage);

        return List.of(CastSkillRespDTO.success(targetTree, damage));
    }

    public int attackDamage(TreeGeneral castPlayer, TreeGeneral targetPlayerTreeGeneral) {
        float attack = castPlayer.getCurrentAttack();
        // 是否暴击
        if (calculationCastSuccessRate((float) castPlayer.getCurrentCrit() / 10f)) {
            attack *= 1.5F;
        }
        Integer currentDefense = targetPlayerTreeGeneral.getCurrentDefense();
        float damage = attack - currentDefense;
        damage = Math.max(damage, 1);
        return Math.round(damage);
    }
}
