package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 示敌以弱
 * 降低目标攻击
 */
public class DeBuffAttackSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "DeBuffSturdy";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentAttack();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentAttack(current - Math.round(add));
        return add;
    }


}
