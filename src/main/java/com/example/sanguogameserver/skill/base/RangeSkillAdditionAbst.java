package com.example.sanguogameserver.skill.base;

import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;

import java.util.ArrayList;
import java.util.List;

public abstract class RangeSkillAdditionAbst implements SkillInterface {
    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        TreeGeneral playerTreeGeneral = castTree.getPlayerTreeGeneral();
        // 扣除消耗
        consume(playerTreeGeneral, skill);


        List<BattleTreeInfoColumn> attackRange = attackRange(castTree, targetTree, trees, skill);
        List<CastSkillRespDTO> castSkillRespDTOList = new ArrayList<>();
        for (BattleTreeInfoColumn treeGeneral : attackRange) {
            float addition = addition(treeGeneral.getPlayerTreeGeneral(), skill);
            castSkillRespDTOList.add(CastSkillRespDTO.success(treeGeneral, addition));

        }
        return castSkillRespDTOList;
    }

    public abstract float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill);

    public List<BattleTreeInfoColumn> attackRange(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, Skill skill) {

        int row = targetTree.getRow();
        int column = targetTree.getColumn();
        List<BattleTreeInfoColumn> treeGenerals = new ArrayList<>();
        // 自身
        BattleTreeInfoColumn selfTree = getColumnByRowAndColumn(trees, row, column);
        if (selfTree.getPlayerTreeGeneral() != null)
            treeGenerals.add(selfTree);

        List<BattleTreeInfoColumn> aroundColumn = getAroundColumn(castTree,trees, row, column);
        treeGenerals.addAll(aroundColumn);

        return treeGenerals;

    }

    ;
}
