package com.example.sanguogameserver.skill.base;

import cn.hutool.core.util.RandomUtil;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;

import java.util.ArrayList;
import java.util.List;

public interface SkillInterface {
    String getSkillCode();

    List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill skill);

    /**
     * 释放技能的消耗
     *
     * @param playerTreeGeneral
     * @param skill
     */
    default void consume(TreeGeneral playerTreeGeneral, Skill skill) {
        playerTreeGeneral.setCurrentMp(playerTreeGeneral.getCurrentMp() - skill.getCosumeMp());
        playerTreeGeneral.setCurrentMp(playerTreeGeneral.getCurrentHealth() - skill.getCosumeHp());
    }

    /**
     * 释放技能是否成功
     *
     * @param successRate
     * @return
     */
    default boolean calculationCastSuccessRate(float successRate) {
        int round = Math.round(successRate * 100);
        int roundNum = RandomUtil.randomInt(0, 100);
        return roundNum < round;
    }

    default BattleTreeInfoColumn getTreeByRowAndColumn(List<BattleTreeInfoRow> trees, int row, int column) {
        if (row < 0 || row >= trees.size()) return null;
        BattleTreeInfoRow battleTreeInfoRow = trees.get(row);
        if (column < 0 || column >= battleTreeInfoRow.getTreeInfoColumns().size()) return null;
        return battleTreeInfoRow.getTreeInfoColumns().get(column);
    }

    default TreeGeneral getPlayerGeneralByRowAndColumn(List<BattleTreeInfoRow> trees, int row, int column) {
        BattleTreeInfoColumn treeByRowAndColumn = getTreeByRowAndColumn(trees, row, column);
        if (treeByRowAndColumn == null) return null;
        if (treeByRowAndColumn.getPlayerTreeGeneral() == null) return null;
        return treeByRowAndColumn.getPlayerTreeGeneral();
    }

    default BattleTreeInfoColumn getColumnByRowAndColumn(List<BattleTreeInfoRow> trees, int row, int column) {
        BattleTreeInfoColumn treeByRowAndColumn = getTreeByRowAndColumn(trees, row, column);
        if (treeByRowAndColumn == null) return null;
        if (treeByRowAndColumn.getPlayerTreeGeneral() == null) return null;
        return treeByRowAndColumn;
    }

    default List<BattleTreeInfoColumn> getAroundColumn(BattleTreeInfoColumn castTree, List<BattleTreeInfoRow> trees, int row, int column) {
        List<BattleTreeInfoColumn> treeGenerals = new ArrayList<>();
        BattleTreeInfoColumn topSelfTree = getColumnByRowAndColumn(trees, row - 1, column);
        if (checkAttackTargetColumn(castTree,topSelfTree))
            treeGenerals.add(topSelfTree);

        // 下
        BattleTreeInfoColumn downSelfTree = getColumnByRowAndColumn(trees, row + 1, column);
        if (checkAttackTargetColumn(castTree,downSelfTree))
            treeGenerals.add(downSelfTree);

        // 左
        BattleTreeInfoColumn leftSelfTree = getColumnByRowAndColumn(trees, row, column - 1);
        if (checkAttackTargetColumn(castTree,leftSelfTree))
            treeGenerals.add(leftSelfTree);

        // 右
        BattleTreeInfoColumn rightSelfTree = getColumnByRowAndColumn(trees, row, column + 1);
        if (checkAttackTargetColumn(castTree,rightSelfTree))
            treeGenerals.add(rightSelfTree);

        return treeGenerals;

    }

    default boolean checkAttackTargetColumn(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetColumn) {
        if (targetColumn == null) return false;
        if (targetColumn.getPlayerTreeGeneral() == null) return false;
        if (castTree.getTreePlayerType() == targetColumn.getTreePlayerType()) return false;
        return true;
    }
}
