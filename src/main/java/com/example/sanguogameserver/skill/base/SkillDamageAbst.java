package com.example.sanguogameserver.skill.base;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;

import java.util.ArrayList;
import java.util.List;

public abstract class SkillDamageAbst implements SkillInterface {
    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        TreeGeneral playerTreeGeneral = castTree.getPlayerTreeGeneral();

        List<BattleTreeInfoColumn> attackRange = attackRange(castTree, targetTree, trees, skill);

        // 扣除消耗
        consume(playerTreeGeneral, skill);

        List<CastSkillRespDTO> castSkillRespDTOS = new ArrayList<>();

        for (BattleTreeInfoColumn treeGeneralColumn : attackRange) {
            CastSkillRespDTO castSkillRespDTO = new CastSkillRespDTO();
            castSkillRespDTO.setAttackTarget(treeGeneralColumn);
            // 没有成功释放
            if (!calculationCastSuccessRate(skill.getSuccessRate())) {
                castSkillRespDTO.setSuccess(false);
            } else {
                int damage = attackDamage(playerTreeGeneral, treeGeneralColumn.getPlayerTreeGeneral(), skill);
                castSkillRespDTO.setDamage(damage);
                treeGeneralColumn.getPlayerTreeGeneral().setCurrentHealth(treeGeneralColumn.getPlayerTreeGeneral().getCurrentHealth() - damage);
                castSkillRespDTO.setSuccess(true);
            }
            castSkillRespDTOS.add(castSkillRespDTO);
        }
        return castSkillRespDTOS;
    }

    public int attackDamage(TreeGeneral castPlayer, TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        float attack = castPlayer.getCurrentSpellStrength() * Convert.toFloat(skill.getGailValue().split(",")[0]);
        Integer currentDefense = targetPlayerTreeGeneral.getCurrentDefense();
        float damage = attack - currentDefense;
        damage = Math.max(damage, 1);
        return Math.round(damage);
    }

    public List<BattleTreeInfoColumn> attackRange(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, Skill skill) {
        return List.of(targetTree);
    }
}
