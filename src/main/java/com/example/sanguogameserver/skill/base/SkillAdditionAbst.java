package com.example.sanguogameserver.skill.base;

import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;

import java.util.List;

public abstract class SkillAdditionAbst implements SkillInterface {
    protected TreeGeneral self;

    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        TreeGeneral playerTreeGeneral = castTree.getPlayerTreeGeneral();
        this.self = playerTreeGeneral;
        // 扣除消耗
        consume(playerTreeGeneral, skill);
        if (!calculationCastSuccessRate(skill.getSuccessRate())) {
            return List.of(CastSkillRespDTO.fail(targetTree));
        }
        float addition = addition(targetTree.getPlayerTreeGeneral(), skill);
        return List.of(CastSkillRespDTO.success(targetTree, addition));
    }

    public abstract float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill);
}
