package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 自身行动范围+1
 */
public class SpeedSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "Speed";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getMoveCount();
        float add = 1;
        targetPlayerTreeGeneral.setMoveCount(current + Math.round(add));
        return add;
    }


}
