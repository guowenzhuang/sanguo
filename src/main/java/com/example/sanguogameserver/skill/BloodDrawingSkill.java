package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillInterface;

import java.util.List;

/**
 * 抽血术
 * Images/skill/BloodDrawing
 */
public class BloodDrawingSkill implements SkillInterface {

    @Override
    public String getSkillCode() {
        return "BloodDrawing";
    }

    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        TreeGeneral playerTreeGeneral = castTree.getPlayerTreeGeneral();
        // 扣除消耗
        consume(playerTreeGeneral, skill);
        if (!calculationCastSuccessRate(skill.getSuccessRate())) {
            return List.of(CastSkillRespDTO.fail(targetTree));
        }
        TreeGeneral targetPlayerTreeGeneral = targetTree.getPlayerTreeGeneral();

        Integer current = playerTreeGeneral.getCurrentSpellStrength();
        float add = current * Convert.toFloat(skill.getGailValue());
        float damage = add - targetPlayerTreeGeneral.getDefense();
        damage = Math.max(damage, 1);
        targetPlayerTreeGeneral.setCurrentHealth(current - Math.round(damage));
        playerTreeGeneral.setCurrentHealth(playerTreeGeneral.getCurrentHealth() + Math.round(damage));
        return List.of(CastSkillRespDTO.success(targetTree, damage));
    }


}
