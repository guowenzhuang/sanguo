package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.enums.AttackElementEnum;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 火强化
 * 火系法术,使友方三回合内攻击附带火元素,额外造成伤害
 */
public class FireEnhancementSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "FireEnhancement";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        // 附加火元素
        targetPlayerTreeGeneral.setAttackElement(AttackElementEnum.Fire);
        targetPlayerTreeGeneral.setAttackElementCount(3);
        return -1f;
    }


}
