package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.GeneralDeBuffer;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillInterface;

import java.util.List;

/**
 * 敌方5回合内不能回血
 */
public class NoReplyHealthSkill implements SkillInterface {

    @Override
    public String getSkillCode() {
        return "NoReplyHealth";
    }

    @Override
    public List<CastSkillRespDTO> castSkill(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, TreeSkill treeSkill) {
        Skill skill = treeSkill.getSkill();
        TreeGeneral playerTreeGeneral = castTree.getPlayerTreeGeneral();
        // 消耗法力
        consume(playerTreeGeneral, skill);
        if (!calculationCastSuccessRate(skill.getSuccessRate())) {
            return List.of(CastSkillRespDTO.fail(targetTree));
        }
        TreeGeneral targetTreePlayerTreeGeneral = targetTree.getPlayerTreeGeneral();
        GeneralDeBuffer generalDeBuffer = new GeneralDeBuffer();
        generalDeBuffer.setGeneralStatusType(GeneralStatusType.NoReplyHealth);
        generalDeBuffer.setCount(5);
        targetTreePlayerTreeGeneral.getBuffer().add(generalDeBuffer);
        return List.of(CastSkillRespDTO.success(targetTree));
    }
}
