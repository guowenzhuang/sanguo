package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.RangeSkillAdditionAbst;

/**
 * 号令之旗
 * 范围友方增加暴击
 */
public class FlagOfOrderSkill extends RangeSkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "FlagOfOrder";
    }


    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentCrit();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentCrit(current + Math.round(add));
        return add;
    }
}
