package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 大回复
 * 大量生命恢复
 */
public class MaxSuppliesHpSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "MaxSuppliesHp";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentHealth();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentHealth(current + Math.round(add));
        return add;
    }


}
