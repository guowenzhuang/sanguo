package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 奋进
 * 增加自己攻击
 */
public class EndeavourSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "Endeavour";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentAttack();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentAttack(current + Math.round(add));
        return add;
    }


}
