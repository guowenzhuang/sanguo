package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.enums.AttackElementEnum;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 冰盾
 * 冰系法术,给友方套上一层冰盾,削弱敌方火系伤害
 */
public class IceShieldSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "IceShield";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        // 附加冰防御
        targetPlayerTreeGeneral.setDefenseElement(AttackElementEnum.Ice);
        targetPlayerTreeGeneral.setDefenseElementCount(999);
        return -1;
    }


}
