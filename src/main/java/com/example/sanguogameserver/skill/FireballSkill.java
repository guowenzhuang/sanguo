package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.skill.base.SkillDamageAbst;

/**
 * 火弹
 * 火系法术,造成单体伤害
 */
public class FireballSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "Fireball";
    }



}
