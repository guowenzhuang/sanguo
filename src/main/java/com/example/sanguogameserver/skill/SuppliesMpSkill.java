package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 回复mp
 */
public class SuppliesMpSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "SuppliesMp";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentMp();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentMp(current + Math.round(add));
        return add;
    }


}
