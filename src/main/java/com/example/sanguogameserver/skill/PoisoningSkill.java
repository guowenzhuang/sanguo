package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.dto.GeneralDeBuffer;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 敌方持续掉血
 */
public class PoisoningSkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "Poisoning";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {

        Integer current = self.getCurrentSpellStrength();
        float add = current * Convert.toFloat(skill.getGailValue());
        float damage = add - targetPlayerTreeGeneral.getDefense();
        damage = Math.max(damage, 1);
        targetPlayerTreeGeneral.setCurrentHealth(targetPlayerTreeGeneral.getCurrentHealth() - Math.round(damage));

        GeneralDeBuffer generalDeBuffer = new GeneralDeBuffer();
        // 中毒
        generalDeBuffer.setGeneralStatusType(GeneralStatusType.poisoning);
        generalDeBuffer.setCount(999);
        targetPlayerTreeGeneral.getBuffer().add(generalDeBuffer);
        return damage;
    }


}
