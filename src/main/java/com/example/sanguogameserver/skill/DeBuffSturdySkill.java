package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 挑拨
 * 降低对方防御
 */
public class DeBuffSturdySkill extends SkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "DeBuffSturdy";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentDefense();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentDefense(current - Math.round(add));
        return add;
    }


}
