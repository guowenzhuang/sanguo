package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.RangeSkillAdditionAbst;

/**
 * 全军出击
 * 范围友方增加攻击
 */
public class AllArmyAttackSkill extends RangeSkillAdditionAbst {

    @Override
    public String getSkillCode() {
        return "AllArmyAttack";
    }


    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentAttack();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentAttack(current + Math.round(add));
        return add;
    }
}
