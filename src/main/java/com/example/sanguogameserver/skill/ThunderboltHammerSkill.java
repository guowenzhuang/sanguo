package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillDamageAbst;

import java.util.ArrayList;
import java.util.List;

/**
 * 雷电法术,造成范围伤害
 */
public class ThunderboltHammerSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "ThunderboltHammer";
    }


    @Override
    public List<BattleTreeInfoColumn> attackRange(BattleTreeInfoColumn castTree, BattleTreeInfoColumn targetTree, List<BattleTreeInfoRow> trees, Skill skill) {
        int row = targetTree.getRow();
        int column = targetTree.getColumn();
        List<BattleTreeInfoColumn> treeGenerals = new ArrayList<>();
        // 自身
        BattleTreeInfoColumn selfTree = getColumnByRowAndColumn(trees, row, column);
        if (selfTree.getPlayerTreeGeneral() != null)
            treeGenerals.add(selfTree);

        List<BattleTreeInfoColumn> aroundColumn = getAroundColumn(castTree,trees, row, column);
        treeGenerals.addAll(aroundColumn);

        return treeGenerals;
    }
}
