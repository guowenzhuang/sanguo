package com.example.sanguogameserver.skill;

import cn.hutool.core.convert.Convert;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillAdditionAbst;

/**
 * 一般生命回复
 */
public class MiddleSuppliesHpSkill extends SkillAdditionAbst {


    @Override
    public String getSkillCode() {
        return "MiddleSuppliesHp";
    }

    @Override
    public float addition(TreeGeneral targetPlayerTreeGeneral, Skill skill) {
        Integer current = targetPlayerTreeGeneral.getCurrentHealth();
        float add = current * Convert.toFloat(skill.getGailValue());
        targetPlayerTreeGeneral.setCurrentHealth(current + Math.round(add));
        return add;
    }


}
