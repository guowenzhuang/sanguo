package com.example.sanguogameserver.skill;

import com.example.sanguogameserver.skill.base.SkillDamageAbst;

/**
 * 雷电法术,造成单体伤害
 */
public class ThunderBallSkill extends SkillDamageAbst {

    @Override
    public String getSkillCode() {
        return "ThunderBall";
    }



}
