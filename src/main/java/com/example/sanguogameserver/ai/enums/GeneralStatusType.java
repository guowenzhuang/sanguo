package com.example.sanguogameserver.ai.enums;

public enum GeneralStatusType {
    // 中毒 降低生命
    poisoning,
    // 钝兵 降低闪避,暴击,
    bluntSoldier,
    // 封咒 不能使用技能
    sealCurse,
    // 混乱,不能做出操作
    chaos,
    // 咒骂,降低对方防御
    curse,
    // 定身 目标不能移动
    sink,
    // 坚固 增加防御
    sturdy,
    // 奋起,增加攻击
    rise,
    // 增加闪避,暴击
    encouraged,
    /**
     * 不能回血
     */
    NoReplyHealth,
    /**
     * 眩晕
     */
    Vertigo,
    /**
     * 灼烧附近敌人
     */
    BurningNearby,
    /**
     * 攻击附带火元素伤害
     */
    FireEnhancement,
    ;

}
