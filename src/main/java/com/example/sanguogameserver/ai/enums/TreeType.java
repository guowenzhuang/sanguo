package com.example.sanguogameserver.ai.enums;

import com.example.sanguogameserver.ai.model.TreeGainSkillType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public enum TreeType {
    // 草地
    grassland,
    // 平原
    plain,
    // 城池 (可以回血)
    city,
    // 石头
    stone,
    ;

    public static List<TreeGainSkillType> gainSkilltypeList(TreeType treeType) {
        List<TreeGainSkillType> gainSkilltypeList = new ArrayList<TreeGainSkillType>();
        TreeGainSkillType gainTreeGainSkillType = new TreeGainSkillType();
        TreeGainSkillType lowerGainTreeGainSkillType = new TreeGainSkillType();

        gainTreeGainSkillType.setGainCoefficient(BigDecimal.valueOf(1.5));
        lowerGainTreeGainSkillType.setGainCoefficient(BigDecimal.valueOf(0.7));

        List<SkillType> gainSkillTypes = new ArrayList<>();
        List<SkillType> lowerGainSkillTypes = new ArrayList<>();
        switch (treeType) {
            case grassland -> {
                gainSkillTypes = List.of(SkillType.fire);
                lowerGainSkillTypes = List.of(SkillType.gold);
            }
            case plain -> {
                gainSkillTypes = List.of(SkillType.soil);
                lowerGainSkillTypes = List.of(SkillType.water);
            }
            case city -> {
                gainSkillTypes = List.of(SkillType.gold);
            }
        }
        gainTreeGainSkillType.setSkillTypes(gainSkillTypes);
        lowerGainTreeGainSkillType.setSkillTypes(lowerGainSkillTypes);
        gainSkilltypeList.add(gainTreeGainSkillType);
        gainSkilltypeList.add(lowerGainTreeGainSkillType);
        return gainSkilltypeList;
    }
}
