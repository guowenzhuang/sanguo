package com.example.sanguogameserver.ai.enums;

public enum SkillType {
    // 风
    wind,
    // 金
    gold,
    // 水
    water,
    // 火
    fire,
    // 土
    soil
}
