package com.example.sanguogameserver.ai.logic;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.skill.base.SkillDamageAbst;
import com.example.sanguogameserver.skill.base.SkillInterface;

import java.util.List;
import java.util.Map;

public class AttackLogic {


    public static TreeSkill attack(TreeGeneral attackTreeGeneral, TreeGeneral beAttackTreeGeneral) {
        // 查询所有的技能
        // 计算普通攻击
        TreeSkill maxDamageTreeSkill = null;

        List<TreeSkill> skills = attackTreeGeneral.getSkills();
        for (TreeSkill skill : skills) {
            // 根据skill查找实现类
            SkillInterface skillInterface = ((Map<String, SkillInterface>) SpringUtil.getBean("skillInterfaces")).get(skill.getSkill().getSkillCode());

            if (skillInterface instanceof SkillDamageAbst skillDamage) {
                int damage = skillDamage.attackDamage(attackTreeGeneral, beAttackTreeGeneral, skill.getSkill());
                if (maxDamageTreeSkill == null || damage > maxDamageTreeSkill.getNormalDamage()) {
                    skill.setNormalDamage(damage);
                    maxDamageTreeSkill = skill;
                }
            }
        }
        return maxDamageTreeSkill;
    }

    public static TreeSkill normalAttack(TreeGeneral attackTreeGeneral, TreeGeneral beAttackTreeGeneral) {
        int damage = attackTreeGeneral.getCurrentAttack() - beAttackTreeGeneral.getCurrentDefense();
        TreeSkill treeSkill = new TreeSkill();
        Skill skill = new Skill();
        skill.setSkillCode("NormalAttackSkill");
        treeSkill.setSkill(skill);
        treeSkill.setNormalAttack(true);
        treeSkill.setNormalDamage(damage);
        return treeSkill;
    }

    public static List<CastSkillRespDTO> attack(BattleTreeInfoColumn attackTreeInfoColumn, BattleTreeInfoColumn beAttackTreeInfoColumn, List<BattleTreeInfoRow> trees, TreeSkill skill) {
        SkillInterface skillInterface = ((Map<String, SkillInterface>) SpringUtil.getBean("skillInterfaces")).get(skill.getSkill().getSkillCode());
        List<CastSkillRespDTO> castSkillRespDTOS = skillInterface.castSkill(attackTreeInfoColumn, beAttackTreeInfoColumn, trees, skill);
        if (CollUtil.isNotEmpty(castSkillRespDTOS)) {
            for (CastSkillRespDTO castSkillRespDTO : castSkillRespDTOS) {
                castSkillRespDTO.setTreeSkill(skill);
            }
        }

        return castSkillRespDTOS;
    }

}
