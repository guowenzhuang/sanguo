package com.example.sanguogameserver.ai.model;

import com.example.sanguogameserver.entity.Skill;
import lombok.Data;

@Data
public class TreeSkill {
    private Skill skill;
    // 是否普通攻击
    private boolean isNormalAttack;

    private int normalDamage;
}
