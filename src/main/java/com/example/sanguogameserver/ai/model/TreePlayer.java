package com.example.sanguogameserver.ai.model;

import lombok.Data;

@Data
public class TreePlayer {
    private int id;

    private String name;
}
