package com.example.sanguogameserver.ai.model;

import com.example.sanguogameserver.ai.enums.SkillType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class TreeGainSkillType {
    /**
     * 增益系数
     */
    private BigDecimal gainCoefficient;
    /**
     * 增益技能
     */
    private List<SkillType> skillTypes;
}
