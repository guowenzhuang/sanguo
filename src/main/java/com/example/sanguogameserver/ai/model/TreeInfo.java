package com.example.sanguogameserver.ai.model;

import com.example.sanguogameserver.ai.enums.TreeType;
import lombok.Data;

import java.util.List;

@Data
public class TreeInfo {
    /**
     * 地区类型
     */
    private TreeType treeType;

    /**
     * 增益技能
     */
    private List<TreeGainSkillType> gainSkilltypes;
    /**
     * 增益武将
     */
    private List<TreeGainGeneralCategory> gainGeneralCategories;

    /**
     * 武将信息
     */
    private TreeGeneral treeGeneral;

    private boolean isPlay;
}
