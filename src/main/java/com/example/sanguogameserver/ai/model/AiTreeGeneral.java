package com.example.sanguogameserver.ai.model;

import com.example.sanguogameserver.ai.enums.AiType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AiTreeGeneral extends TreeGeneral {
    /**
     * ai类型
     */
    private AiType aiType;
}
