package com.example.sanguogameserver.ai.model;

import com.example.sanguogameserver.ai.enums.AiType;
import com.example.sanguogameserver.dto.GeneralDeBuffer;
import com.example.sanguogameserver.enums.AttackElementEnum;
import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import com.example.sanguogameserver.enums.MutationTypeEnum;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TreeGeneral {
    private Integer id;

    private Integer playId;

    private Integer generalId;

    /**
     * 血量
     */
    private Integer currentHealth;

    private Integer health;

    /**
     * 血量
     */
    private Integer currentMp;

    /**
     * 法术蓝条
     */
    private Integer mp;

    /**
     * 武将类型
     */
    private GeneralCategoryEnum generalCategoryEnum;

    /**
     * 武将名称
     */
    private String generalName;

    /**
     * 攻击力
     */
    private Integer currentAttack;
    /**
     * 攻击力
     */
    private Integer attack;

    /**
     * 防御
     */
    private Integer currentDefense;

    /**
     * 防御
     */
    private Integer defense;

    /**
     * 法术强度
     */
    private Integer currentSpellStrength;

    /**
     * 法术强度
     */
    private Integer spellStrength;

    /**
     * 闪避率
     */
    private Integer currentDodge;

    /**
     * 闪避率
     */
    private Integer dodge;

    /**
     * 暴击率
     */
    private Integer currentCrit;

    /**
     * 暴击率
     */
    private Integer crit;

    private AttackElementEnum attackElement;

    private int attackElementCount;

    private AttackElementEnum defenseElement;

    private int defenseElementCount;

    private List<GeneralDeBuffer> buffer = new ArrayList<>();

    /**
     * 星级
     */
    private String star;

    /**
     * 经验值
     */
    private Integer level;


    /**
     * 武将类型
     */
    private String generalType;

    private MutationTypeEnum mutationType;

    private String range;

    private int rangeCount;

    private int skillRangeCount;

    private int moveCount;

    /**
     * 武将归属 红方/蓝方
     */
    private TreePlayerTypeEnum treePlayerType;

    /**
     * 所有的技能
     */
    private List<TreeSkill> skills;

    private TreeSkill currentRoundingSkill;

    /**
     * ai类型
     */
    private AiType aiType;

    private boolean isAi;

    /**
     * 当前回合是否已移动
     */
    private boolean currentMoved = false;
    /**
     * 当前回合是否被禁锢,不能移动,但是可以施法
     */
    private boolean currentImprisonment = false;

    /**
     * 禁止回复血量
     */
    private boolean currentBanReplayHealth;

    /**
     * 当前回合是否眩晕
     */
    private boolean currentVertigo;
}
