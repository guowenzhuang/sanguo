package com.example.sanguogameserver.ai.model;

import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import lombok.Data;

import java.util.List;

@Data
public class TreeGainGeneralCategory {
    /**
     * 增益行动力
     */
    private int addRange;
    /**
     * 增益技能
     */
    private List<GeneralCategoryEnum> generalCategories;
}
