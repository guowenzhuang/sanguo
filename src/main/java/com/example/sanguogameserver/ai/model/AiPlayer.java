package com.example.sanguogameserver.ai.model;

import lombok.Data;

@Data
public class AiPlayer {
    private String name;
}
