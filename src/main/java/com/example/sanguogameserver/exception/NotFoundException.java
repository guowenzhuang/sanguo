package com.example.sanguogameserver.exception;

import lombok.Data;
import org.slf4j.MDC;

@Data
public class NotFoundException extends RuntimeException{
    private String requestId;

    public NotFoundException(String errorMessage){
        super(errorMessage);
        setRequestId(MDC.get("requestId"));
    }
}