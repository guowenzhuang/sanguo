package com.example.sanguogameserver.config;

import cn.hutool.core.util.StrUtil;
import com.example.sanguogameserver.consts.DateTimeFormatConst;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Configuration
public class ISODateConfig {


    /**
     * Jackson序列化和反序列化转换器，用于转换Post请求体中的json以及将对象序列化为返回响应的json
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> builder
                .serializerByType(LocalDateTime.class,
                        new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_DATETIME_PATTERN)))

                .serializerByType(LocalDate.class,
                        new LocalDateSerializer(DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_DATE_PATTERN)))

                .serializerByType(LocalTime.class,
                        new LocalTimeSerializer(DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_TIME_PATTERN)))

                .serializerByType(Date.class, new DateSerializer(false, new SimpleDateFormat(DateTimeFormatConst.DEFAULT_DATETIME_PATTERN)))

                .deserializerByType(LocalDateTime.class,
                        new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_DATETIME_PATTERN)))

                .deserializerByType(LocalDate.class,
                        new LocalDateDeserializer(DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_DATE_PATTERN)))

                .deserializerByType(LocalTime.class,
                        new LocalTimeDeserializer(DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_TIME_PATTERN)))

                .deserializerByType(Date.class,
                        new DateDeserializers.DateDeserializer(DateDeserializers.DateDeserializer.instance,
                                new SimpleDateFormat(DateTimeFormatConst.DEFAULT_DATETIME_PATTERN), DateTimeFormatConst.DEFAULT_DATETIME_PATTERN))

                .featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
    }



    /**
     * get方式的参数 转换
     * String->Date
     *
     * @return
     */
    @Bean
    public Converter<String, Date> DateConvert() {
        return new Converter<String, Date>() {
            @Override
            public Date convert(String source) {
                if (StrUtil.isEmpty(source)) {
                    return null;
                }
                try {
                    return new SimpleDateFormat(DateTimeFormatConst.DEFAULT_DATETIME_PATTERN).parse(source);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                throw new IllegalArgumentException("string解析失败:" + source);
            }

        };
    }

    /**
     * get方式的参数 转换
     * String->LocalDateTime
     *
     * @return
     */
    @Bean
    public Converter<String, LocalDateTime> LocalDateTimeConvert() {
        return new Converter<String, LocalDateTime>() {
            @Override
            public LocalDateTime convert(String source) {
                if (StrUtil.isEmpty(source)) {
                    return null;
                }
                return LocalDateTime.parse(source, DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_DATETIME_PATTERN));
            }

        };
    }

    /**
     * get方式的参数 转换
     * String->LocalDate
     *
     * @return
     */
    @Bean
    public Converter<String, LocalDate> LocalDateConvert() {
        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String source) {
                if (StrUtil.isEmpty(source)) {
                    return null;
                }
                return LocalDate.parse(source, DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_DATE_PATTERN));
            }

        };
    }

    /**
     * get方式的参数 转换
     * String->LocalTime
     *
     * @return
     */
    @Bean
    public Converter<String, LocalTime> LocalTimeConvert() {
        return new Converter<String, LocalTime>() {
            @Override
            public LocalTime convert(String source) {
                if (StrUtil.isEmpty(source)) {
                    return null;
                }
                return LocalTime.parse(source, DateTimeFormatter.ofPattern(DateTimeFormatConst.DEFAULT_TIME_PATTERN));
            }

        };
    }

}
