package com.example.sanguogameserver.config;

import com.example.sanguogameserver.exception.NotFoundException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import java.util.Map;

public class MyErrorAttributes extends DefaultErrorAttributes {


    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
        throw new NotFoundException("找不到资源 " + ((ServletWebRequest) webRequest).getRequest().getRequestURI());
    }
}