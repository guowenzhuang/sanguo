package com.example.sanguogameserver.config;

import com.example.sanguogameserver.logic.generalStatus.GeneraStatusInterface;
import com.example.sanguogameserver.skill.base.SkillInterface;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Configuration
@Slf4j
public class AttackBeanInitConfig {


    @Bean
    public Map<String, SkillInterface> skillInterfaces() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Map<String, SkillInterface> map = new HashMap<>();
        // 指定要扫描的包名
        Reflections reflections = new Reflections("com.example.sanguogameserver.skill");

        // 获取某接口的所有实现类
        Set<Class<? extends SkillInterface>> implementations = reflections.getSubTypesOf(SkillInterface.class);

        // 输出所有实现类
        for (Class<? extends SkillInterface> impl : implementations) {
            if (!impl.isInterface() && !java.lang.reflect.Modifier.isAbstract(impl.getModifiers())) {

                Constructor<? extends SkillInterface> constructor = impl.getConstructor();
                // 调用无参构造方法
                SkillInterface bean = constructor.newInstance();
                map.put(bean.getSkillCode(), bean);
            }
        }
        log.info("加载所有技能实现类:{}",map);
        return map;
    }

    @Bean
    public Map<String, GeneraStatusInterface> generaStatusInterfaces() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Map<String, GeneraStatusInterface> map = new HashMap<>();
        // 指定要扫描的包名
        Reflections reflections = new Reflections("com.example.sanguogameserver.logic.generalStatus");

        // 获取某接口的所有实现类
        Set<Class<? extends GeneraStatusInterface>> implementations = reflections.getSubTypesOf(GeneraStatusInterface.class);

        // 输出所有实现类
        for (Class<? extends GeneraStatusInterface> impl : implementations) {
            if (!impl.isInterface() && !java.lang.reflect.Modifier.isAbstract(impl.getModifiers())) {

                Constructor<? extends GeneraStatusInterface> constructor = impl.getConstructor();
                // 调用无参构造方法
                GeneraStatusInterface bean = constructor.newInstance();
                map.put(bean.getGeneralStatus().name(), bean);
            }
        }
        return map;
    }
}
