package com.example.sanguogameserver.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "reponse-advice")
@Data
public class ReponseAdviceConfig {
    /**
     * 忽略url
     */
    private String ignoreUrls;


}
