package com.example.sanguogameserver.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.handlers.Fastjson2TypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.example.sanguogameserver.dto.battle.BattleDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.ibatis.type.JdbcType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {


    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        /**
         * 多租户
         */
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));

        ObjectMapper objectMapper = new ObjectMapper();
        // 注册Java时间模块以支持Java 8日期和时间API
        objectMapper.registerModule(new JavaTimeModule());
        // 对于其他自定义配置，可以继续在这里添加
        // 例如，要忽略未知的JSON字段，可以使用：
        // objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JacksonTypeHandler.setObjectMapper(objectMapper);

        return interceptor;
    }
    @Bean
    public ConfigurationCustomizer mybatisConfigurationCustomizer() {
        return configuration -> {
            // 注册自定义 TypeHandler
            configuration.getTypeHandlerRegistry().register(JdbcType.JAVA_OBJECT, new Fastjson2TypeHandler(BattleDetail.class));
        };
    }
}