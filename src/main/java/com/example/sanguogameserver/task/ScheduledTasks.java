package com.example.sanguogameserver.task;

import cn.hutool.core.date.DateUtil;
import com.example.sanguogameserver.service.AttackTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ScheduledTasks {

    @Autowired
    private AttackTaskService attackTaskService;

    @Scheduled(cron = "0 * * * * ?")
    public void updateAttackTaskStatus() {
        // 应该战斗的任务 // 提前十五分钟
        attackTaskService.updateAttackTaskStatus();
    }
}