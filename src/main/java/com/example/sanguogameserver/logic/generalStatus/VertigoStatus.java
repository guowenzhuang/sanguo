package com.example.sanguogameserver.logic.generalStatus;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;

import java.util.List;

public class VertigoStatus implements GeneraStatusInterface {
    @Override
    public GeneralStatusType getGeneralStatus() {
        return GeneralStatusType.Vertigo;
    }

    @Override
    public List<CastSkillRespDTO> trigger(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees) {
        if (iswokeUp()) {
            removeBuffer(triggerColumn);
            return List.of(CastSkillRespDTO.fail(triggerColumn));
        } else {
            triggerColumn.getPlayerTreeGeneral().setCurrentVertigo(true);
            return List.of(CastSkillRespDTO.success(triggerColumn));
        }

    }

}
