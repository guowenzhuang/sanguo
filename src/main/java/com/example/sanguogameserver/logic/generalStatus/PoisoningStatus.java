package com.example.sanguogameserver.logic.generalStatus;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;

import java.util.ArrayList;
import java.util.List;

public class PoisoningStatus implements GeneraStatusInterface {
    @Override
    public GeneralStatusType getGeneralStatus() {
        return GeneralStatusType.poisoning;
    }

    @Override
    public List<CastSkillRespDTO> trigger(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees) {
        List<CastSkillRespDTO> castSkillRespDTOs = new ArrayList<>();
        // 醒来, 不受伤害
        if (iswokeUp()) {
            CastSkillRespDTO castSkillRespDTO = CastSkillRespDTO.fail(triggerColumn);
            castSkillRespDTOs.add(castSkillRespDTO);
            removeBuffer(triggerColumn);
        } else {
            int damage = 10;
            TreeGeneral playerTreeGeneral = triggerColumn.getPlayerTreeGeneral();
            playerTreeGeneral.setCurrentHealth(playerTreeGeneral.getCurrentHealth() - damage);
            CastSkillRespDTO castSkillRespDTO = CastSkillRespDTO.success(triggerColumn, damage);
            castSkillRespDTOs.add(castSkillRespDTO);
        }
        return castSkillRespDTOs;
    }

}
