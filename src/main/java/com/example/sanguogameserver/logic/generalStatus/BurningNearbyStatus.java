package com.example.sanguogameserver.logic.generalStatus;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;

import java.util.ArrayList;
import java.util.List;

public class BurningNearbyStatus implements GeneraStatusInterface {
    @Override
    public GeneralStatusType getGeneralStatus() {
        return GeneralStatusType.BurningNearby;
    }

    @Override
    public List<CastSkillRespDTO> trigger(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees) {
        List<BattleTreeInfoColumn> nearbyPlayerGeneral = getNearbyPlayerGeneral(triggerColumn, trees);
        TreePlayerTypeEnum triggerTreePlayerType = triggerColumn.getTreePlayerType();
        CastSkillRespDTO result = new CastSkillRespDTO();
        result.setSuccess(true);
        result.setAttackPlayerGreen(true);

        List<CastSkillRespDTO> castSkillRespDTOs = new ArrayList<>();
        for (BattleTreeInfoColumn battleTreeInfoColumn : nearbyPlayerGeneral) {
            // 自己人跳过
            if (triggerTreePlayerType == battleTreeInfoColumn.getTreePlayerType()) {
                continue;
            }
            // 醒来, 不受伤害
            if (iswokeUp()) {
                CastSkillRespDTO castSkillRespDTO = CastSkillRespDTO.fail(battleTreeInfoColumn);
                castSkillRespDTOs.add(castSkillRespDTO);
            } else {
                int damage = triggerColumn.getPlayerTreeGeneral().getLevel() * 10;
                TreeGeneral playerTreeGeneral = battleTreeInfoColumn.getPlayerTreeGeneral();
                playerTreeGeneral.setCurrentHealth(playerTreeGeneral.getCurrentHealth() - damage);
                CastSkillRespDTO castSkillRespDTO = CastSkillRespDTO.success(battleTreeInfoColumn, damage);
                castSkillRespDTOs.add(castSkillRespDTO);
            }
        }
        result.setAttackOtherTarget(castSkillRespDTOs);
        return List.of(result);
    }

}
