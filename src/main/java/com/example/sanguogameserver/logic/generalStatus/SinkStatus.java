package com.example.sanguogameserver.logic.generalStatus;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.GeneralDeBuffer;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;

import java.util.ArrayList;
import java.util.List;

public class SinkStatus implements GeneraStatusInterface {
    @Override
    public GeneralStatusType getGeneralStatus() {
        return GeneralStatusType.sink;
    }

    @Override
    public List<CastSkillRespDTO> trigger(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees) {
        if (iswokeUp()) {
            removeBuffer(triggerColumn);
            return List.of(CastSkillRespDTO.fail(triggerColumn));
        } else {
            triggerColumn.getPlayerTreeGeneral().setCurrentImprisonment(true);
            return List.of(CastSkillRespDTO.success(triggerColumn));
        }

    }

}
