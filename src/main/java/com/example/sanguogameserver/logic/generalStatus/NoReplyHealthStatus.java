package com.example.sanguogameserver.logic.generalStatus;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;

import java.util.List;

public class NoReplyHealthStatus implements GeneraStatusInterface {
    @Override
    public GeneralStatusType getGeneralStatus() {
        return GeneralStatusType.NoReplyHealth;
    }

    @Override
    public List<CastSkillRespDTO> trigger(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees) {

        triggerColumn.getPlayerTreeGeneral().setCurrentBanReplayHealth(true);
        return List.of(CastSkillRespDTO.success(triggerColumn));
    }

}
