package com.example.sanguogameserver.logic.generalStatus;

import cn.hutool.core.util.RandomUtil;
import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.dto.GeneralDeBuffer;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 灼烧附近敌人
 */
public interface GeneraStatusInterface {

    GeneralStatusType getGeneralStatus();

    List<CastSkillRespDTO> trigger(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees);

    default List<BattleTreeInfoColumn> getNearbyPlayerGeneral(BattleTreeInfoColumn triggerColumn, List<BattleTreeInfoRow> trees) {
        int row = triggerColumn.getRow();
        int column = triggerColumn.getColumn();
        List<BattleTreeInfoColumn> treeGenerals = new ArrayList<BattleTreeInfoColumn>();
        // 上
        BattleTreeInfoColumn topSelfTree = getColumnByRowAndColumn(trees, row - 1, column);
        if (topSelfTree.getPlayerTreeGeneral() != null)
            treeGenerals.add(topSelfTree);

        // 下
        BattleTreeInfoColumn downSelfTree = getColumnByRowAndColumn(trees, row + 1, column);
        if (downSelfTree.getPlayerTreeGeneral() != null)
            treeGenerals.add(downSelfTree);

        // 左
        BattleTreeInfoColumn leftSelfTree = getColumnByRowAndColumn(trees, row, column - 1);
        if (leftSelfTree.getPlayerTreeGeneral() != null)
            treeGenerals.add(leftSelfTree);

        // 右
        BattleTreeInfoColumn rightSelfTree = getColumnByRowAndColumn(trees, row, column + 1);
        if (rightSelfTree.getPlayerTreeGeneral() != null)
            treeGenerals.add(rightSelfTree);
        return treeGenerals;
    }

    default BattleTreeInfoColumn getTreeByRowAndColumn(List<BattleTreeInfoRow> trees, int row, int column) {
        if (row < 0 || row >= trees.size()) return null;
        BattleTreeInfoRow battleTreeInfoRow = trees.get(row);
        if (column < 0 || column >= battleTreeInfoRow.getTreeInfoColumns().size()) return null;
        return battleTreeInfoRow.getTreeInfoColumns().get(column);
    }

    default BattleTreeInfoColumn getColumnByRowAndColumn(List<BattleTreeInfoRow> trees, int row, int column) {
        BattleTreeInfoColumn treeByRowAndColumn = getTreeByRowAndColumn(trees, row, column);
        if (treeByRowAndColumn == null) return null;
        if (treeByRowAndColumn.getPlayerTreeGeneral() == null) return null;
        return treeByRowAndColumn;
    }

    /**
     * 是否醒来
     *
     * @return
     */
    default boolean iswokeUp() {
        return RandomUtil.randomBoolean();
    }

    default void removeBuffer(BattleTreeInfoColumn triggerColumn) {
        List<GeneralDeBuffer> buffer = triggerColumn.getPlayerTreeGeneral().getBuffer();
        triggerColumn.getPlayerTreeGeneral().setBuffer(buffer
                .stream()
                        .filter(item -> item.getGeneralStatusType() != getGeneralStatus())
                .toList());
    }
}
