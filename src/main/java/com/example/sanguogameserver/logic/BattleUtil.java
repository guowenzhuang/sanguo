package com.example.sanguogameserver.logic;

import com.example.sanguogameserver.ai.enums.AiType;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.entity.AttackTask;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;

public class BattleUtil {
    public static TreePlayerTypeEnum getTreePlayerType(AttackTask attackTask, Integer playerId) {
        if (attackTask.getAttackPlayerId().equals(playerId)) {
            return TreePlayerTypeEnum.red;
        }
        return TreePlayerTypeEnum.blue;
    }

    public static void setAi(BattleTreeInfoColumn battleTreeInfoColumn, boolean isAi, AiType aiType) {
        battleTreeInfoColumn.setAi(isAi);
        if (battleTreeInfoColumn.getPlayerTreeGeneral() != null) {
            battleTreeInfoColumn.getPlayerTreeGeneral().setAi(isAi);
            battleTreeInfoColumn.getPlayerTreeGeneral().setAiType(aiType);
        }
    }
}
