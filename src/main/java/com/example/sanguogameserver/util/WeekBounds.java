package com.example.sanguogameserver.util;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class WeekBounds {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();

        // get first day of this year
        LocalDate startOfYear = today.with(TemporalAdjusters.firstDayOfYear());

        // get start of the last week
        LocalDate endOfLastWeek = today.minusWeeks(1)
                .with(WeekFields.of(Locale.getDefault()).dayOfWeek(), 7);

        LocalDate startOfWeek = startOfYear;

        while (!startOfWeek.isAfter(endOfLastWeek)) {
            LocalDate endOfWeek = startOfWeek.with(WeekFields.of(Locale.getDefault()).dayOfWeek(), 7);

            // If endOfWeek is after endOfLastWeek, set endOfWeek to endOfLastWeek.
            if (endOfWeek.isAfter(endOfLastWeek)) {
                endOfWeek = endOfLastWeek;
            }

            System.out.println("Period for the week: "
                    + startOfWeek + " - " + endOfWeek);

            // Move to the next week.
            startOfWeek = endOfWeek.plusDays(1);
        }
    }

}
