package com.example.sanguogameserver.util;

import org.springframework.stereotype.Component;

@Component
public class LevelUtil {
    private final Integer expCoefficient = 10;
    private final Integer mutationCoefficient = 5;

    public Long getNextExp(int level, boolean isMutation, int star) {
        long max = Math.max((long) (((long) level * level * level + (5L * level)) * expCoefficient - 80), 0);
        max = max * star;
        if (isMutation) {
            max = max * mutationCoefficient;
        }
        return max;
    }
}
