package com.example.sanguogameserver.util;

import com.example.sanguogameserver.dto.Message;
import com.example.sanguogameserver.entity.Chat;
import com.example.sanguogameserver.enums.EnableEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChatUtil {

    public static Chat conversionChar(Message message, Integer receivePlayerId) {
        Chat chat = new Chat();
        chat.setMessageId(message.getMessageId());
        chat.setPlayerAId(message.getSendPlayerId());
        chat.setPlayerBId(receivePlayerId);
        chat.setMessage(message.getMessage());
        chat.setSenderId(message.getSendPlayerId());
        chat.setCreateDate(message.getSendDate());
        chat.setReadStatus(EnableEnum.False);
        chat.setGroupId(message.getGroupId());
        chat.setChatType(message.getChatType());
        chat.setSendHead(message.getSendHead());
        chat.setSendName(message.getSendName());
        return chat;
    }

    public static Message conversionMessage(Chat chat) {
        Message message = new Message();
        message.setMessageId(chat.getMessageId());
        message.setSendPlayerId(chat.getSenderId());
        message.setMessage(chat.getMessage());
        message.setSendDate(chat.getCreateDate());
        message.setGroupId(message.getGroupId());
        message.setChatType(message.getChatType());
        message.setSendHead(chat.getSendHead());
        message.setSendName(chat.getSendName());
        return message;
    }

}
