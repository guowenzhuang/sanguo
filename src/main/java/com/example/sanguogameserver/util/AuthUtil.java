package com.example.sanguogameserver.util;

import com.example.sanguogameserver.consts.RequestAttributeKeyConst;
import com.example.sanguogameserver.dto.CurrentPlayer;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.HashMap;
import java.util.Map;

public class AuthUtil {

    private final static ThreadLocal<Map<String, Object>> holder = new ThreadLocal<>();

    /**
     * 获取当前用户
     */
    public static CurrentPlayer getCurrPlayer() {
        HttpServletRequest request = ContextUtil.getRequest();
        if (request == null) {
            return null;
        }
        CurrentPlayer competenceUser = (CurrentPlayer) request.getAttribute(RequestAttributeKeyConst.USERINFO);
        return competenceUser;
    }



    /**
     * 获取当前用户id
     *
     * @return
     */
    public static Integer getCurrPlayerId() {
        CurrentPlayer currUser = getCurrPlayer();
        if (currUser == null)
            return null;
        return currUser.getId();
    }
    public static Integer getCurrServerId() {
        CurrentPlayer currUser = getCurrPlayer();
        if (currUser == null)
            return null;
        return currUser.getServerId();
    }
    public static String getZhuLuId() {
        CurrentPlayer currUser = getCurrPlayer();
        if (currUser == null)
            return null;
        return currUser.getZhuLuId();
    }




    /**
     * 获取当前用户昵称
     *
     * @return
     */
    public static String getCurrPlayerNickName() {
        CurrentPlayer currUser = getCurrPlayer();
        if (currUser == null)
            return null;
        return currUser.getPlayName();
    }

    public static String getToken() {
        HttpServletRequest request = ContextUtil.getRequest();
        return getToken(request);
    }

    public static String getToken(HttpServletRequest request) {
        String token = request.getParameter("token");
        if (token == null)
            token = request.getHeader("token");
        return token;
    }


}
