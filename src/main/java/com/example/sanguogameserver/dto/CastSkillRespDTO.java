package com.example.sanguogameserver.dto;

import com.example.sanguogameserver.ai.model.TreeSkill;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.entity.Skill;
import lombok.Data;

import java.util.List;

@Data
public class CastSkillRespDTO {
    private boolean isSuccess = true;
    /**
     * 随机攻击的时候,攻击到了谁
     */
    private BattleTreeInfoColumn attackTarget;
    /**
     * 受到的伤害
     */
    private float damage;

    /**
     * 是否是攻击武将
     */
    private boolean isAttackPlayerGreen;

    private TreeSkill treeSkill;
    /**
     * 攻击到的其他人
     */
    private List<CastSkillRespDTO> attackOtherTarget;

    /**
     * 反击
     */
    private List<CastSkillRespDTO> counterattackCastSkill;


    public static CastSkillRespDTO success() {
        return new CastSkillRespDTO();
    }

    public static CastSkillRespDTO success(BattleTreeInfoColumn attackTarget) {
        CastSkillRespDTO castSkillRespDTO = new CastSkillRespDTO();
        castSkillRespDTO.setAttackTarget(attackTarget);
        return castSkillRespDTO;
    }

    public static CastSkillRespDTO success(BattleTreeInfoColumn attackTarget, float damage) {
        CastSkillRespDTO castSkillRespDTO = new CastSkillRespDTO();
        castSkillRespDTO.setAttackTarget(attackTarget);
        castSkillRespDTO.setDamage(damage);
        return castSkillRespDTO;
    }

    public static CastSkillRespDTO fail() {
        CastSkillRespDTO castSkillRespDTO = new CastSkillRespDTO();
        castSkillRespDTO.setSuccess(false);
        return castSkillRespDTO;
    }

    public static CastSkillRespDTO fail(BattleTreeInfoColumn attackTarget) {
        CastSkillRespDTO castSkillRespDTO = fail();
        castSkillRespDTO.setAttackTarget(attackTarget);
        return castSkillRespDTO;
    }
}
