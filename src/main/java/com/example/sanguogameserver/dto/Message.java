package com.example.sanguogameserver.dto;

import com.example.sanguogameserver.enums.ChatTypeEnum;
import com.example.sanguogameserver.enums.HeadEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Message {

    private String messageId;
    /**
     * 发送人id
     */
    private Integer sendPlayerId;
    /**
     * 发送消息内容
     */
    private String message;
    /**
     * 发送时间
     */
    private LocalDateTime sendDate;


    private String groupId;

    private ChatTypeEnum chatType;

    private HeadEnum sendHead;

    private String sendName;
}
