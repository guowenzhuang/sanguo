package com.example.sanguogameserver.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MessageDTO {
    private List<Message> messages = new ArrayList<>();
}
