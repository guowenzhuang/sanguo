package com.example.sanguogameserver.dto.battle;

import com.example.sanguogameserver.dto.CastSkillRespDTO;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import lombok.Data;

import java.util.List;

@Data
public class BattleRoundInfo {

    /**
     * 序号
     */
    private int stepId = 1;

    /**
     * 第几回合
     */
    private int round;


    private TreePlayerTypeEnum treePlayerTypeEnum;

    private boolean isAi;

    private boolean isRoundInit;

    /**
     * 是否战斗结束
     */
    private boolean gameOver;

    /**
     * 胜利方(1:red 2:blue 3:平局)
     */
    private int win;


    /**
     * 释放技能响应
     */
    private List<CastSkillRespDTO> castSkills;

    private AiRoundInfo aiRoundInfo;


}
