package com.example.sanguogameserver.dto.battle;

import lombok.Data;

@Data
public class StartBattleReq {
    private Integer attackTaskId;
}
