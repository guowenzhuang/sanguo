package com.example.sanguogameserver.dto.battle;

import com.example.sanguogameserver.ai.model.TreeSkill;
import lombok.Data;

@Data
public class AiRoundInfo {

    private Integer playerTreeGeneralId;
    /**
     * 旧
     */
    private Integer oldRow;
    /**
     * 旧
     */
    private Integer oldColumn;
    /**
     * 新
     */
    private Integer newRow;
    /**
     * 新
     */
    private Integer newColumn;
    /**
     * 选中的
     */
    private Integer selectRow;
    /**
     * 选中的
     */
    private Integer selectColumn;
    /**
     * 释放技能
     */
    private TreeSkill treeSkill;
}
