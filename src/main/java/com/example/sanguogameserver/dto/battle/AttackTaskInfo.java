package com.example.sanguogameserver.dto.battle;

import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.enums.BattleEnum;
import com.example.sanguogameserver.enums.EnableEnum;
import com.example.sanguogameserver.enums.ProcessEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AttackTaskInfo {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 攻打时间
     */
    private LocalDateTime arrivalTime;

    /**
     * 准备战斗时间
     */
    private LocalDateTime prepareAttackTime;

    /**
     * 是否已处理
     */
    private ProcessEnum process;

    /**
     * 攻击玩家id
     */
    private Integer attackPlayerId;

    /**
     * 被攻击玩家id
     */
    private Integer beAttackPlayerId;

    /**
     * 领地id
     */
    private Integer beAttackTerritoryId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 是否正在战斗
     */
    private BattleEnum isBattle;
    /**
     * 攻击玩家是否进入
     */
    private EnableEnum attackIsEnter;
    /**
     * 攻击玩家是否进入
     */
    private EnableEnum beAttackIsEnter;

    private Player attackPlayer;

    private Player beAttackPlayer;
}
