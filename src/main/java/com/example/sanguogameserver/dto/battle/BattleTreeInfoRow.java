package com.example.sanguogameserver.dto.battle;

import lombok.Data;

import java.util.List;

@Data
public class BattleTreeInfoRow {
    /**
     *
     */
    private List<BattleTreeInfoColumn> treeInfoColumns;


    /**
     * 当前在第几行
     */
    private int row;
}
