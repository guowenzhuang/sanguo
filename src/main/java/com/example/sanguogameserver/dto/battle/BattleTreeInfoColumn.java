package com.example.sanguogameserver.dto.battle;

import com.example.sanguogameserver.ai.enums.TreeType;
import com.example.sanguogameserver.ai.model.TreeGainGeneralCategory;
import com.example.sanguogameserver.ai.model.TreeGainSkillType;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BattleTreeInfoColumn {
    /**
     * 地区类型
     */
    private TreeType treeType;

    /**
     * 增益技能
     */
    private List<TreeGainSkillType> gainSkillTypes = new ArrayList<>();
    /**
     * 增益武将类别,//FIXME 暂时为空,不根据地形判断武将增益
     */
    private List<TreeGainGeneralCategory> gainGeneralCategories = new ArrayList<>();

    /**
     * 武将信息
     */
    private TreeGeneral playerTreeGeneral;

    private TreeGeneral deadPlayerTreeGeneral;

    /**
     * 武将归属 红方/蓝方
     */
    private TreePlayerTypeEnum treePlayerType;

    private boolean isAi;

    /**
     * 是否可以移动
     */
    private Boolean isMove;


    /**
     * 当前在第几行
     */
    private int row;

    /**
     * 当前在第几列
     */
    private int column;

    /**
     * 行动分数,最大的移动
     */
    private int goScore = 0;

    /**
     * 攻击分数,
     */
    private int beAttackScore = 0;

    private String testPrintStr;

    private boolean moveRange;

    private boolean attackRange;

    private boolean playerRange;

    public void init() {
        this.gainSkillTypes.clear();
        this.gainGeneralCategories.clear();
        this.playerTreeGeneral = null;
        this.treePlayerType = null;
        this.isAi = false;
        this.isMove = true;
        this.goScore = 0;
        this.beAttackScore = 0;
        this.testPrintStr = null;
        this.moveRange = false;
        this.attackRange = false;
        this.playerRange = false;
        if (this.treeType != null) {
            gainTreeType(this.treeType, this);
            // 是否可以移动
            if(this.treeType == TreeType.stone){
                this.isMove=false;
            }
        }
    }

    public static void gainTreeType(TreeType treeType, BattleTreeInfoColumn battleTreeInfoColumn) {
        battleTreeInfoColumn.gainSkillTypes = TreeType.gainSkilltypeList(treeType);
    }
}
