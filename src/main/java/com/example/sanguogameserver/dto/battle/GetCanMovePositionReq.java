package com.example.sanguogameserver.dto.battle;

import lombok.Data;

@Data
public class GetCanMovePositionReq {
    private Integer id;
    /**
     * 第几行
     */
    private Integer row;
    /**
     * 第几列
     */
    private Integer column;
}
