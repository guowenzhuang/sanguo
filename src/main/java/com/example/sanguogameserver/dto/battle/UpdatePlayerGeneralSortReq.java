package com.example.sanguogameserver.dto.battle;

import lombok.Data;

@Data
public class UpdatePlayerGeneralSortReq {
    /**
     * 修改武将
     */
    private Integer attackTaskId;

    /**
     * 交换武将id
     */
    private Integer exchangePlayerGeneralId;

    /**
     * 被交换武将id
     */
    private Integer beExchangePlayerGeneralId;
}
