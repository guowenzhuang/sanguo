package com.example.sanguogameserver.dto.battle;

import cn.hutool.core.collection.CollUtil;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

@Data
public class BattleDetail {
    /**
     * 信息
     */
    public List<BattleTreeInfoRow> trees;
    /**
     * 谁走
     */
    private TreePlayerTypeEnum currentPlayerType;

    /**
     * 当前回合信息
     */
    private List<BattleRoundInfo> battleRoundInfos = new ArrayList<>();


    private boolean attackIsAi;
    private boolean beAttackIsAi;

    private int round;

    private Date roundRemainderDate;

    private AttackTaskInfo attackTask;

    private int stepId;

    /**
     * 是否战斗结束
     */
    private boolean gameOver;

    /**
     * 胜利方(1:red 2:blue 3:平局)
     */
    private int win;

    public int getStepId() {
        if(CollUtil.isNotEmpty(battleRoundInfos)){
            return battleRoundInfos.getLast().getStepId();
        }
        return 1;
    }

    public List<BattleTreeInfoColumn> findWhere(Predicate<BattleTreeInfoColumn> predicate) {
        List<BattleTreeInfoColumn> result = new ArrayList<>();
        for (BattleTreeInfoRow tree : trees) {
            for (BattleTreeInfoColumn treeInfoColumn : tree.getTreeInfoColumns()) {
                if (predicate.test(treeInfoColumn)) {
                    result.add(treeInfoColumn);
                }
            }
        }
        return result;
    }

}
