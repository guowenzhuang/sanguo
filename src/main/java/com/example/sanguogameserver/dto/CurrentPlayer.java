package com.example.sanguogameserver.dto;

import com.example.sanguogameserver.enums.HeadEnum;
import com.example.sanguogameserver.enums.TitleEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CurrentPlayer implements Serializable {
    private Integer id;

    private HeadEnum head;

    private String zhuLuId;

    private String playName;

    private String countryName;

    private String flagColor;

    private TitleEnum title;

    private Integer territoryNum;

    private Integer resourcesTotal;

    private Integer serverId;

    private Integer userId;

    private LocalDateTime createDate;
}
