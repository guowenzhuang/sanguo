package com.example.sanguogameserver.dto;

import lombok.Data;

@Data
public class PlayerGeneralStartSumDTO {
    private int playerGeneralStart1Sum;

    private int playerGeneralStart2Sum;

    private int playerGeneralStart3Sum;

    private int playerGeneralStart4Sum;

    private int playerGeneralStart5Sum;
}
