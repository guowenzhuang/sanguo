package com.example.sanguogameserver.dto;

import com.example.sanguogameserver.ai.enums.GeneralStatusType;
import lombok.Data;

@Data
public class GeneralDeBuffer {
    private GeneralStatusType generalStatusType;
    private int count;
}
