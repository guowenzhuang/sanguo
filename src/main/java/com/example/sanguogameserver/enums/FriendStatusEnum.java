package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FriendStatusEnum implements IEnum<String> {
    //
    application("申请"),
    rejected("被拒绝"),
    normal("正常"),
    black("拉黑"),
    delete("删除"),
    notResponding("未响应"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
