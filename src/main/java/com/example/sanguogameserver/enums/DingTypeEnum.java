package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DingTypeEnum implements IEnum<String> {
    //
    normal("正常"),
    //doNotDisturb("doNotDisturb"),
    specialReminder("specialReminder"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
