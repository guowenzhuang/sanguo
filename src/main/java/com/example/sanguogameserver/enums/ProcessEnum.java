package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ProcessEnum implements IEnum<String> {
    //
    True("已处理"),
    False("未处理"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
