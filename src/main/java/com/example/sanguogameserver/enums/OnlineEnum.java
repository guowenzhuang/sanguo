package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OnlineEnum implements IEnum<String> {
    // 盾兵
    online("在线"),
    Offline("离线");


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
