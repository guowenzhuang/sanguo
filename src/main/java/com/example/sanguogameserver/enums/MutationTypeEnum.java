package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MutationTypeEnum implements IEnum<String> {
    ordinary("正常"),
    dark("黑暗变异"),
    ;
    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
