package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BattleEnum implements IEnum<String> {
    //
    didnTStart("未开始"),
    todoBattle("准备战斗"),
    inBattle("战斗中"),
    theBattleIsOver("战斗结束"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
