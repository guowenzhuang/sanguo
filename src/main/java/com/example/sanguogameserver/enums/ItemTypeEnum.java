package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ItemTypeEnum implements IEnum<String> {
    //类型(1:道具,2:经验书,3:武将碎片,4:回复hp 5: 回复mp)
    props("道具"),
    experienceBook("经验书"),
    militaryShard("武将碎片"),
    replyToHP("回复hp"),
    replyToMp("回复mp");

    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
    }
