package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.bind.annotation.GetMapping;

@Getter
@AllArgsConstructor
public enum TitleEnum implements IEnum<String> {
    LV1("初出茅庐");

    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
