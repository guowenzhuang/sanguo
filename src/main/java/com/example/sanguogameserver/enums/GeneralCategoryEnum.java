package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GeneralCategoryEnum implements IEnum<String> {
    // 盾兵
    shieldSoldier("盾兵"),
    // 战士
    warrior("战士"),
    // 法师
    mage("法师"),
    // 牧师
    priest("牧师"),
    // 射手
    shooter("射手");

    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
