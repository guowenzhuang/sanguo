package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SkillCategoryEnum implements IEnum<String> {

    health("回血"),
    mp("回mp"),
    curse("咒"),
    attack("强化攻击"),
    crit("强化暴击"),
    defense("强化防御"),
    dodge("强化闪避"),
    move("增加移动"),
    damage("伤害"),
    debuff("降低对方状态"),
    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
