package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EnableEnum implements IEnum<String> {
    //
    True("启用"),
    False("关闭"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
