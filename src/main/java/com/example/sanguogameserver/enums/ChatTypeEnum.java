package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ChatTypeEnum implements IEnum<String> {
    //
    privateChat("私聊"),
    groupChat("群聊"),
    world("世界"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
