package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum HeadEnum implements IEnum<String> {
    Head1("001"),
    Head2("002"),
    Head3("003"),
    Head4("004"),
    Head5("005"),
    Head6("006"),
    Head7("007"),
    Head8("008"),
    Head9("009"),
    Head10("010"),
    Head11("011"),
    Head12("012"),
    Head13("013"),
    Head14("014"),
    Head15("015"),
    Head16("016"),
    Head17("017"),
    Head18("018"),
    Head19("019"),
    Head20("020"),
    Head21("021"),
    Head22("022"),
    Head23("023"),
    Head24("024"),
    Head25("025"),
    Head26("026"),
    Head27("027"),
    Head28("028"),
    Head29("029"),
    Head30("030"),
    Head31("031"),
    Head32("032"),
    Head33("033"),
    Head34("034"),
    Head35("035"),
    Head36("036"),
    Head37("037"),
    Head38("038"),
    Head39("039"),
    Head40("040");
    ;
    @JsonValue
    private final String resourcePath;

    @Override
    public String getValue() {
        return this.resourcePath;
    }
}
