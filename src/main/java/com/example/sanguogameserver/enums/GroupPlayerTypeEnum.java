package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GroupPlayerTypeEnum implements IEnum<String> {
    //
    groupMaster("群主"),
    //administrator("管理员"),
    ordinary("普通"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
