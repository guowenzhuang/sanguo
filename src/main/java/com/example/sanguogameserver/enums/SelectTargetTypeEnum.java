package com.example.sanguogameserver.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SelectTargetTypeEnum implements IEnum<String> {
    //
    self("自己"),
    selfFriends("自己友方"),
    friends("友方"),
    enemy("敌人"),

    ;


    private final String desc;

    @Override
    public String getValue() {
        return this.name();
    }
}
