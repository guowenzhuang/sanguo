package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.SkillGeneralType;
import com.example.sanguogameserver.mapper.SkillGeneralTypeMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 武将类型技能表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@Service
public class SkillGeneralTypeService extends ServiceImpl<SkillGeneralTypeMapper, SkillGeneralType> implements IService<SkillGeneralType> {

}
