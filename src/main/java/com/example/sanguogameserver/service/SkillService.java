package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.mapper.SkillMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 技能 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@Service
public class SkillService extends ServiceImpl<SkillMapper, Skill> implements IService<Skill> {

    /**
     * 随机抽取一个技能
     *
     * @return
     */
    public Skill raffleSkill() {
        Integer id = baseMapper.randomSkillRow();
        return getById(id);
    }

    public List<Skill> getByCodes(List<String> skillCodes) {
        return
                lambdaQuery()
                        .in(Skill::getSkillCode, skillCodes)
                        .list();
    }

    public Skill getByCode(String skillCode) {
        return
                lambdaQuery()
                        .eq(Skill::getSkillCode, skillCode)
                        .last("limit 1")
                        .one();
    }
}
