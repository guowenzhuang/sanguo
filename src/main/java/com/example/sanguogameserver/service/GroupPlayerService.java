package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.GroupPlayer;
import com.example.sanguogameserver.mapper.GroupPlayerMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 群成员表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
@Service
public class GroupPlayerService extends ServiceImpl<GroupPlayerMapper, GroupPlayer> implements IService<GroupPlayer> {

}
