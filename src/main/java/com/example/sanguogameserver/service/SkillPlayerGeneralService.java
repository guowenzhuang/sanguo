package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.entity.SkillPlayerGeneral;
import com.example.sanguogameserver.mapper.SkillPlayerGeneralMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 玩家武将技能表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@Service
public class SkillPlayerGeneralService extends ServiceImpl<SkillPlayerGeneralMapper, SkillPlayerGeneral> implements IService<SkillPlayerGeneral> {

    public List<SkillPlayerGeneral> listByPlayerGeneralId(Integer playerGeneralId) {
        return
                lambdaQuery()
                        .eq(SkillPlayerGeneral::getPlayerGeneralId, playerGeneralId)
                        .list();
    }
}
