package com.example.sanguogameserver.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.entity.PlayerGeneral;
import com.example.sanguogameserver.entity.Territory;
import com.example.sanguogameserver.mapper.TerritoryMapper;
import com.example.sanguogameserver.request.OccupiedTerritoryReq;
import com.example.sanguogameserver.request.PlayerGeneralIdIndex;
import com.example.sanguogameserver.response.TerritoryAllResp;
import com.example.sanguogameserver.util.AuthUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@Service
public class TerritoryService extends ServiceImpl<TerritoryMapper, Territory> implements IService<Territory> {

    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerGeneralService playerGeneralService;

    public List<TerritoryAllResp> listAll() {
        List<Territory> territoryList = list();
        return territoryList.stream().map(item -> {
            TerritoryAllResp territoryAllResp = new TerritoryAllResp();
            BeanUtils.copyProperties(item, territoryAllResp);
            if (item.getPlayId() != null)
                territoryAllResp.setPlayer(playerService.getById(item.getPlayId()));

            if (item.getPlayerGeneralId() != null)
                territoryAllResp.setPlayerGeneral(playerGeneralService.getById(item.getPlayerGeneralId()));

            return territoryAllResp;
        }).collect(Collectors.toList());
    }

    public List<Territory> listByPlayerId(Integer playerId) {
        return lambdaQuery()
                .eq(Territory::getPlayId, playerId)
                .list();
    }

    @Transactional
    public Player occupied(OccupiedTerritoryReq occupiedTerritoryReq) {
        Territory territory = getById(occupiedTerritoryReq.getTerritoryId());
        if (territory.getPlayId() != null) {
            Player player = playerService.getById(territory.getPlayId());
            throw new IllegalArgumentException(StrUtil.format("已被{}占领", player.getPlayName()));
        }
        territory.setPlayId(AuthUtil.getCurrPlayerId());
        updateById(territory);
        for (PlayerGeneralIdIndex playerGeneralIdIndex : occupiedTerritoryReq.getPlayerGenerals()) {
            PlayerGeneral playerGeneral = playerGeneralService.getById(playerGeneralIdIndex.getId());
            playerGeneral.setTerritoryIndex(playerGeneralIdIndex.getIndex());
            playerGeneral.setTerritoryId(territory.getId());
            playerGeneralService.updateById(playerGeneral);
        }
        return playerService.getById(AuthUtil.getCurrPlayerId());
    }
}
