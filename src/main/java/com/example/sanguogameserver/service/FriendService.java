package com.example.sanguogameserver.service;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.dto.CurrentPlayer;
import com.example.sanguogameserver.entity.Friend;
import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.enums.FriendStatusEnum;
import com.example.sanguogameserver.mapper.FriendMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.sanguogameserver.request.AddFriendReq;
import com.example.sanguogameserver.request.ProcessingAddFriendListReq;
import com.example.sanguogameserver.response.AddFriendListResp;
import com.example.sanguogameserver.response.FriendGroupResp;
import com.example.sanguogameserver.response.FriendListResp;
import com.example.sanguogameserver.util.AuthUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-25
 */
@Service
public class FriendService extends ServiceImpl<FriendMapper, Friend> implements IService<Friend> {

    @Autowired
    private PlayerService playerService;

    public void addFriend(AddFriendReq addFriendReq) {
        Assert.isTrue(!addFriendReq.getZhuLuId().equals(AuthUtil.getZhuLuId()), "不能添加自己");
        Player friendPlayer = playerService.getByZhuLuId(addFriendReq.getZhuLuId());
        Assert.notNull(friendPlayer, "未找到玩家");
        Friend friend = getByTwoPlayerId(AuthUtil.getCurrPlayerId(), friendPlayer.getId());
        if (friend != null) {
            Assert.isFalse(friend.getFriendStatus() == FriendStatusEnum.application, "已经发起申请,耐心等待吧");
            Assert.isFalse(friend.getFriendStatus() == FriendStatusEnum.normal, "你们已经是好友了");
            Assert.isFalse(friend.getFriendStatus() == FriendStatusEnum.black, "被拉黑了");
        }
        friend = new Friend();
        friend.setHead(friendPlayer.getHead());
        friend.setMePlayerId(AuthUtil.getCurrPlayerId());
        friend.setZhuLuId(friendPlayer.getZhuLuId());
        friend.setPlayerId(friendPlayer.getId());
        friend.setNickName(friendPlayer.getPlayName());
        friend.setCreateDate(LocalDateTime.now());
        friend.setIntimacy(0);
        friend.setFriendGroup("我的好友");
        friend.setFriendStatus(FriendStatusEnum.application);
        saveOrUpdate(friend);
    }

    public Friend getByTwoPlayerId(Integer mePlayerId, Integer friendPlayerId) {
        return baseMapper.getByTwoPlayerId(mePlayerId, friendPlayerId);
    }

    public List<AddFriendListResp> addFriendList() {
        return baseMapper.addFriendList(AuthUtil.getCurrPlayerId());
    }

    public void processingAddFriendList(ProcessingAddFriendListReq processingAddFriendListReq) {
        Friend friend = getById(processingAddFriendListReq.getFriendId());
        friend.setFriendStatus(processingAddFriendListReq.getFriendStatus());
        updateById(friend);
    }

    public List<FriendGroupResp> friendList() {
        List<FriendGroupResp> friendGroupResps = baseMapper.friendList(AuthUtil.getCurrPlayerId());

        FriendGroupResp friendGroupResp = new FriendGroupResp();
        friendGroupResp.setGroup("待添加好友");
        List<AddFriendListResp> addFriendListResps = addFriendList();
        friendGroupResp.setFriends(
                addFriendListResps.stream().map(item -> {
                    FriendListResp friendListResp = new FriendListResp();
                    BeanUtils.copyProperties(item,friendListResp);
                    return friendListResp;
                }).collect(Collectors.toList())
        );
        friendGroupResps.add(friendGroupResp);
        return friendGroupResps;
    }

    public List<Friend> isFriends(Integer playerId, List<Integer> friendPlayerId) {
        return baseMapper.getFriends(playerId, friendPlayerId);
    }
}
