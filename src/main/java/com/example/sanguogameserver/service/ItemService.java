package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.Item;
import com.example.sanguogameserver.enums.ItemTypeEnum;
import com.example.sanguogameserver.mapper.ItemMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 道具 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-18
 */
@Service
public class ItemService extends ServiceImpl<ItemMapper, Item> implements IService<Item> {

    public Item getByGeneralFragment(Integer generalId) {
        return lambdaQuery()
                .eq(Item::getGeneralId,generalId)
                .eq(Item::getItemType, ItemTypeEnum.militaryShard)
                .last("limit 1")
                .one();
    }
}
