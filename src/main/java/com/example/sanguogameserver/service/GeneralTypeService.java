package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.GeneralType;
import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import com.example.sanguogameserver.enums.MutationTypeEnum;
import com.example.sanguogameserver.mapper.GeneralTypeMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.swing.plaf.PanelUI;

/**
 * <p>
 * 武将类型 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
@Service
public class GeneralTypeService extends ServiceImpl<GeneralTypeMapper, GeneralType> implements IService<GeneralType> {

    public GeneralType getInitGeneral(GeneralCategoryEnum generalCategory, MutationTypeEnum mutationType) {
        return lambdaQuery()
                .eq(GeneralType::getCategory, generalCategory)
                .eq(GeneralType::getParentId, 0)
                .eq(GeneralType::getMutationType, mutationType)
                .last("limit 1")
                .one();
    }

    public GeneralType getGeneralByCategoryAndName(GeneralCategoryEnum generalCategory, String name) {
        return lambdaQuery()
                .eq(GeneralType::getCategory, generalCategory)
                .eq(GeneralType::getName, name)
                .last("limit 1")
                .one();
    }
}
