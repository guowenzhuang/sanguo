package com.example.sanguogameserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.sanguogameserver.entity.General;
import com.example.sanguogameserver.mapper.GeneralMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@Service
public class GeneralService extends ServiceImpl<GeneralMapper, General> implements IService<General> {

}
