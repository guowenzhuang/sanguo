package com.example.sanguogameserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.example.sanguogameserver.mapper")
@EnableScheduling
@Import(cn.hutool.extra.spring.SpringUtil.class)
@EnableAsync
public class SanguoGameServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SanguoGameServerApplication.class, args);
    }

}
