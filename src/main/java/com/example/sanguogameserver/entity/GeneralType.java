package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;

import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import com.example.sanguogameserver.enums.MutationTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 武将类型
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
@Getter
@Setter
@TableName("general_type")
public class GeneralType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 范围 示例[[1,1],[-1,1]]
     */
    @TableField("`range`")
    private String range;

    /**
     * 攻击范围数字，滚动几次
     */
    private Integer rangeCount;

    private Integer skillRangeCount;

    /**
     * 上层兵种，由什么进化
     */
    private Integer parentId;

    /**
     * 下层兵种,进化成什么
     */
    private Integer nextId;

    /**
     * 变异类型，ordinary/dark
     */
    private MutationTypeEnum mutationType;

    /**
     * 分类
     */
    private GeneralCategoryEnum category;

    /**
     * 变异id
     */
    private Integer mutationId;

    /**
     * 血量增幅
     */
    private BigDecimal healthIncreaseRate;

    /**
     * 蓝量增幅
     */
    private BigDecimal mpIncreaseRate;

    /**
     * 攻击增幅
     */
    private BigDecimal attackIncreaseRate;

    /**
     * 防御增幅
     */
    private BigDecimal defenseIncreaseRate;

    /**
     * 法术强度增幅
     */
    private BigDecimal spellStrengthIncreaseRate;

    /**
     * 闪避率增幅
     */
    private BigDecimal dodgeIncreaseRate;

    /**
     * 暴击率增幅
     */
    private BigDecimal critIncreaseRate;

    private Integer moveCount;
}
