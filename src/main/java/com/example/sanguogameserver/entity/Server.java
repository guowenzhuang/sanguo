package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@Data
public class Server implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer serverNo;

    private String serverName;

    private String region;

    /**
     * 1正常,2:拥堵,3:爆满 0:维护
     */
    private Integer serverStatus;
}
