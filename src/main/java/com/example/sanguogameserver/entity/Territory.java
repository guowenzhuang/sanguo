package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.example.sanguogameserver.dto.battle.BattleDetail;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@TableName(value = "territory",autoResultMap = true)
@Data
public class Territory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String territoryName;

    private Integer resourcesResidualTotal;

    private Integer resourcesSeconds;

    private Integer playId;

    private Float positionX;

    private Float positionY;

    private Integer level;

    /**
     * 主驻守武将id
     */
    private Integer playerGeneralId;

    /**
     * 武将最大参战人数
     */
    private Integer generalMaxCount;

    /**
     * 总行数
     */
    private Integer totalRow;

    /**
     * 总列数
     */
    private Integer totalColumn;

    @TableField(typeHandler = JacksonTypeHandler.class)
    private BattleDetail treeData;
}
