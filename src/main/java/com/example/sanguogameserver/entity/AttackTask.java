package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.Fastjson2TypeHandler;
import com.example.sanguogameserver.dto.battle.BattleDetail;
import com.example.sanguogameserver.enums.BattleEnum;
import com.example.sanguogameserver.enums.EnableEnum;
import com.example.sanguogameserver.enums.ProcessEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 攻打任务
 * </p>
 *
 * @author baomidou
 * @since 2024-05-15
 */
@Getter
@Setter
@TableName(value = "attack_task", autoResultMap = true)
public class AttackTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 攻打时间
     */
    private LocalDateTime arrivalTime;

    /**
     * 准备战斗时间
     */
    private LocalDateTime prepareAttackTime;

    /**
     * 是否已处理
     */
    private ProcessEnum process;

    /**
     * 攻击玩家id
     */
    private Integer attackPlayerId;

    /**
     * 被攻击玩家id
     */
    private Integer beAttackPlayerId;

    /**
     * 领地id
     */
    private Integer beAttackTerritoryId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 是否正在战斗
     */
    private BattleEnum isBattle;
    /**
     * 攻击玩家是否进入
     */
    private EnableEnum attackIsEnter;
    /**
     * 攻击玩家是否进入
     */
    private EnableEnum beAttackIsEnter;

    /**
     * 胜利者(0:未结束 1:红方赢,2:蓝方赢,3:平局)
     */
    private int win;

    @TableField(typeHandler = Fastjson2TypeHandler.class, value = "attack_detail_json")
    private BattleDetail attackDetailJson;
}
