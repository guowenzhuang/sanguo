package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 玩家武将技能表
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@Getter
@Setter
@TableName("skill_player_general")
public class SkillPlayerGeneral implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 玩家武将id
     */
    private Integer playerGeneralId;

    /**
     * 技能标识
     */
    private String skillCode;

    /**
     * 技能名称
     */
    private String skillName;

    /**
     * 技能路径
     */
    private String skillIcon;
}
