package com.example.sanguogameserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.sanguogameserver.enums.ChatTypeEnum;
import com.example.sanguogameserver.enums.EnableEnum;
import com.example.sanguogameserver.enums.HeadEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author baomidou
 * @since 2024-03-26
 */
@Getter
@Setter
public class Chat implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String messageId;

    /**
     * 玩家1id
     */
    private Integer playerAId;

    /**
     * 玩家2id
     */
    private Integer playerBId;

    private HeadEnum sendHead;

    private String sendName;
    /**
     * 聊天内容
     */
    private String message;

    /**
     * 发送者id
     */
    private Integer senderId;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    private EnableEnum readStatus;

    private String groupId;

    private ChatTypeEnum chatType;
}
