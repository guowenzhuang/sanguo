package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 武将类型技能表
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@Getter
@Setter
@TableName("skill_general_type")
public class SkillGeneralType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 武将类型id
     */
    private Integer generalTypeId;

    /**
     * 技能标识
     */
    private String skillCode;

    /**
     * 技能名称
     */
    private Integer skillName;

    /**
     * 技能路径
     */
    private String skillIcon;
}
