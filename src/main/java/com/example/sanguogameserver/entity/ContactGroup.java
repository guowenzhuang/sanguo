package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 群组表
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
@Getter
@Setter
@TableName("contact_group")
public class ContactGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 群id
     */
    private String groupId;

    /**
     * 名称
     */
    private String groupName;

    /**
     * 群公告
     */
    private String groupNotice;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
