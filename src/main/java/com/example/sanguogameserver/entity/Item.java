package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.example.sanguogameserver.enums.ItemTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 道具
 * </p>
 *
 * @author baomidou
 * @since 2024-03-18
 */
@Getter
@Setter
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String itemName;

    /**
     * 介绍
     */
    private String itemDesc;

    /**
     * 类型(1:道具,2:经验书,3:武将碎片)
     */
    private ItemTypeEnum itemType;

    private String quantity;

    private Integer generalId;
}
