package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.sanguogameserver.enums.HeadEnum;
import com.example.sanguogameserver.enums.OnlineEnum;
import com.example.sanguogameserver.enums.TitleEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
/**
 * <p>
 *
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private HeadEnum head;

    private String playName;

    private String countryName;
    /**
     * 逐鹿id
     */
    private String zhuLuId;

    private String flagColor;

    private TitleEnum title;

    private Integer territoryNum;

    private Integer resourcesTotal;

    private Integer serverId;

    private Integer userId;

    private LocalDateTime createDate;

    private OnlineEnum online;

    private Integer attackTaskId;


}
