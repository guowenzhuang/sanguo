package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;

import com.example.sanguogameserver.enums.SelectTargetTypeEnum;
import com.example.sanguogameserver.enums.SkillCategoryEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 技能
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@Getter
@Setter
public class Skill implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 技能标识
     */
    private String skillCode;

    /**
     * 技能名称
     */
    private String skillName;

    /**
     * 技能路径
     */
    private String skillIcon;

    /**
     * 技能介绍
     */
    private String skillDesc;

    /**
     * 消耗法力
     */
    private Integer cosumeMp;

    /**
     * 消耗hp
     */
    private Integer cosumeHp;

    /**
     * 技能分类
     */
    private SkillCategoryEnum skillCategory;

    /**
     * 增益value
     */
    private String gailValue;

    /**
     * 释放目标,友方/敌方
     */
    private String targetType;

    private float successRate;
}
