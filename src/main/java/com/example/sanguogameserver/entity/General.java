package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
@Getter
@Setter
public class General implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 血量
     */
    private Integer health;

    /**
     * 法术蓝条
     */
    private Integer mp;

    /**
     * 武将类型
     */
    private GeneralCategoryEnum generalCategory;

    /**
     * 武将名称
     */
    private String generalName;

    /**
     * 攻击力
     */
    private Integer attack;

    /**
     * 防御
     */
    private Integer defense;

    /**
     * 法术强度
     */
    private Integer spellStrength;

    /**
     * 闪避率
     */
    private Integer dodge;

    /**
     * 暴击率
     */
    private Integer crit;

    /**
     * 武将描述
     */
    private String generalDesc;

    /**
     * 星级
     */
    private String star;

    /**
     * 资源位置
     */
    private String assertResourcesPath;
}
