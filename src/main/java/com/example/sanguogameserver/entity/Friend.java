package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.example.sanguogameserver.enums.FriendStatusEnum;
import com.example.sanguogameserver.enums.HeadEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author baomidou
 * @since 2024-03-26
 */
@Getter
@Setter
public class Friend implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 头像
     */
    private HeadEnum head;

    /**
     * 玩家id
     */
    private Integer mePlayerId;

    /**
     * 逐鹿id
     */
    private String zhuLuId;

    /**
     * 好友id
     */
    private Integer playerId;

    /**
     * 好友昵称
     */
    private String nickName;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 亲密度值
     */
    private Integer intimacy;

    /**
     * 好友状态
     */
    private FriendStatusEnum friendStatus;

    /**
     * 好友分组
     */
    private String friendGroup;
}
