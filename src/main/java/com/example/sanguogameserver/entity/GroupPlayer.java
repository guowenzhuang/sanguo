package com.example.sanguogameserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.example.sanguogameserver.enums.DingTypeEnum;
import com.example.sanguogameserver.enums.GroupPlayerTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 群成员表
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
@Getter
@Setter
@TableName("group_player")
public class GroupPlayer implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 群id
     */
    private String groupId;

    /**
     * 名称
     */
    private String groupName;

    /**
     * 玩家id
     */
    private Integer playerId;

    /**
     * 玩家昵称
     */
    private String playerName;

    /**
     * 玩家类型(群主,管理员,普通)
     */
    private GroupPlayerTypeEnum groupPlayerType;

    /**
     * 消息提醒类型(正常,免打扰,特别提醒)
     */
    private DingTypeEnum dingType;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
