package com.example.sanguogameserver.consts;

public class SanGuoConst {
    // 残血百分比
    public static final float residualBlood = 0.3f;

    public static final int groupHeadNum = 4;

    // 战斗剩余时间5分钟
    public static final int attackRoundSecond = 5 * 60;

    public static class RedisLock {
        public static final String messagePlayerId = "MESSAGEREAD";
        public static final String messageWorldServerId = "MESSAGEWORLDSERVERID";
        public static final String registerPlayer = "REGISTERPLAYER";
    }
}
