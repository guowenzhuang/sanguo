package com.example.sanguogameserver.consts;

import java.text.SimpleDateFormat;

public class DateTimeFormatConst {
    public final static SimpleDateFormat yyyyMMddHHmmssSSS = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    /**
     * DEFAULT_DATETIME_PATTERN/Date格式化字符串
     */
    public static final String DEFAULT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * LocalDate格式化字符串
     */
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

    /**
     * LocalTime格式化字符串
     */
    public static final String DEFAULT_TIME_PATTERN = "HH:mm:ss";
}