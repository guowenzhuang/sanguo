package com.example.sanguogameserver.consts;

import java.time.Duration;

/**
 * redis key
 */
public enum RedisKeyConst {
    /**
     * 权限缓存时间由配置文件设置
     */
    AUTH(Duration.ofDays(30)),
    /**
     * 刷新权限缓存时间由配置文件设置
     */
    REFRESH(null),

    Chat(Duration.ofHours(8)),
    ChatWORLD(Duration.ofHours(8)),
    DeleteHash(null),
    Online(Duration.ofMinutes(5)),
    ;


    private Duration duration;

    RedisKeyConst(Duration duration) {
        this.duration = duration;
    }


    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
