package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.request.CreateGroupReq;
import com.example.sanguogameserver.request.PullPlayerGroupReq;
import com.example.sanguogameserver.response.GroupListResp;
import com.example.sanguogameserver.service.ContactGroupService;
import com.example.sanguogameserver.util.AuthUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 群组表 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/api/contactGroup")
public class ContactGroupController {

    @Autowired
    private ContactGroupService contactGroupService;

    /**
     * 群列表
     *
     * @return
     */
    @GetMapping("/list")
    public List<GroupListResp> list() {
        return contactGroupService.listByPlayerId(AuthUtil.getCurrPlayerId());
    }

    /**
     * 创建群
     *
     * @param createGroupReq
     * @return
     */
    @PostMapping("/create")
    public GroupListResp create(@RequestBody CreateGroupReq createGroupReq) {
        return contactGroupService.create(createGroupReq);
    }

    /**
     * 拉人进群
     *
     * @param pullPlayerGroupReq
     */
    @PostMapping("/pullPlayer")
    public void pullPlayer(@RequestBody PullPlayerGroupReq pullPlayerGroupReq) {
        contactGroupService.pullPlayer(pullPlayerGroupReq);
    }

    /**
     * 获取群详情
     *
     * @param id
     * @return
     */
    @GetMapping("/getById")
    public GroupListResp getById(Integer id) {
        return contactGroupService.detailById(id);
    }

}
