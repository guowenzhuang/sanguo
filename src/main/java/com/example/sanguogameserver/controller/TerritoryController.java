package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.request.OccupiedTerritoryReq;
import com.example.sanguogameserver.response.TerritoryAllResp;
import com.example.sanguogameserver.service.TerritoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@RestController
@RequestMapping("/api/territory")
public class TerritoryController {

    @Autowired
    private TerritoryService territoryService;

    @GetMapping("/listAll")
    public List<TerritoryAllResp> listAll() {
        return territoryService.listAll();
    }

    @PostMapping("/occupied")
    public Player occupied(@RequestBody OccupiedTerritoryReq occupiedTerritoryReq) {
        return territoryService.occupied(occupiedTerritoryReq);
    }
}
