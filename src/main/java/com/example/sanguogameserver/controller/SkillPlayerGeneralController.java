package com.example.sanguogameserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 玩家武将技能表 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@RestController
@RequestMapping("/api/skillPlayerGeneral")
public class SkillPlayerGeneralController {

}
