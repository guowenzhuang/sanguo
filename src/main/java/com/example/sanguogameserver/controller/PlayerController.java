package com.example.sanguogameserver.controller;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.example.sanguogameserver.annotations.IgnoreAuth;
import com.example.sanguogameserver.dto.CurrentPlayer;
import com.example.sanguogameserver.enums.HeadEnum;
import com.example.sanguogameserver.request.PlayerRegister;
import com.example.sanguogameserver.request.UpdatePlayerInfo;
import com.example.sanguogameserver.response.MeInfoResp;
import com.example.sanguogameserver.response.SimplePlayerInfoResp;
import com.example.sanguogameserver.service.PlayerService;
import com.example.sanguogameserver.util.AuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@RestController
@RequestMapping("/api/player")
public class PlayerController {
    @Autowired
    private PlayerService playerService;

    @GetMapping("/enterServer")
    @IgnoreAuth
    public String enterServer(Integer userId, Integer serverId) {
        return playerService.enterServer(userId, serverId);
    }

    @PostMapping("/registerPlayer")
    @IgnoreAuth
    public String registerPlayer(@RequestBody PlayerRegister playerRegister) {
        return playerService.registerPlayer(playerRegister);
    }

    @GetMapping("/me")
    public CurrentPlayer me() {
        return AuthUtil.getCurrPlayer();
    }


    @GetMapping("/searchPlayer")
    public List<SimplePlayerInfoResp> findPlayerByName(String searchContent) {
        return playerService.findPlayerByName(searchContent);
    }

    @GetMapping("/meInfo")
    public MeInfoResp meInfo() {
        return playerService.playerInfo(AuthUtil.getCurrPlayerId());
    }

    /**
     * 上线
     */
    @GetMapping("/online")
    public void online() {
        playerService.online();
    }

    @PostMapping("/update")
    public void updatePlayerInfo(@RequestBody @Validated UpdatePlayerInfo updatePlayerInfo) {
        playerService.updatePlayerInfo(updatePlayerInfo);
    }

    @GetMapping("/allHead")
    public HeadEnum[] allHead() {
        return HeadEnum.values();
    }

    /**
     * 发放奖励
     */
    @PostMapping("/rewardResourcesTotal")
    public Integer rewardResourcesTotal() {
       return playerService.rewardResourcesTotal();
    }

    /**
     * 退出登录
     */
    @PostMapping("/exit")
    public void exit() {
        playerService.exit();
    }
}
