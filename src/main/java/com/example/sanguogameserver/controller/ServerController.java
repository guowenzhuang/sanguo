package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.annotations.IgnoreAuth;
import com.example.sanguogameserver.entity.Server;
import com.example.sanguogameserver.request.UserRegister;
import com.example.sanguogameserver.response.ServerUserRes;
import com.example.sanguogameserver.response.base.Result;
import com.example.sanguogameserver.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@RestController
@RequestMapping("/api/server")
public class ServerController {

    @Autowired
    private ServerService serverService;
    @GetMapping("/listByUserId")
    @IgnoreAuth
    public ServerUserRes listByUserId(@RequestParam Integer userId) {
        return serverService.list(userId);
    }

}
