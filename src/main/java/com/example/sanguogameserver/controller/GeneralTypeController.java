package com.example.sanguogameserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 武将类型 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
@RestController
@RequestMapping("/generalType")
public class GeneralTypeController {

}
