package com.example.sanguogameserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 武将类型技能表 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@RestController
@RequestMapping("/api/skillGeneralType")
public class SkillGeneralTypeController {

}
