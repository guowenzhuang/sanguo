package com.example.sanguogameserver.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.sanguogameserver.entity.PlayerGeneral;
import com.example.sanguogameserver.entity.PlayerItem;
import com.example.sanguogameserver.request.*;
import com.example.sanguogameserver.response.PlayerGeneralResp;
import com.example.sanguogameserver.response.RaffleResp;
import com.example.sanguogameserver.response.TerrainAndUnTerrainResp;
import com.example.sanguogameserver.service.PlayerGeneralService;
import com.example.sanguogameserver.util.AuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 玩家武将 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
@RestController
@RequestMapping("/api/playerGeneral")
public class PlayerGeneralController {
    @Autowired
    private PlayerGeneralService playerGeneralService;

    /**
     * 抽奖
     *
     * @return
     */
    @GetMapping("/raffle")
    public RaffleResp raffle(RaffleRequest raffleRequest) {
        return playerGeneralService.raffle(raffleRequest);
    }

    /**
     * 列表
     *
     * @param playerGeneralPageReq
     * @return
     */
    @PostMapping("/page")
    public Page<PlayerGeneralResp> page(@RequestBody PlayerGeneralPageReq playerGeneralPageReq) {
        return playerGeneralService.pageRecord(playerGeneralPageReq);
    }

    @GetMapping("/detailById/{id}")
    public PlayerGeneralResp detailById(@PathVariable("id") Integer id) {
        return playerGeneralService.detailById(id);
    }

    @PostMapping("/previousOrNext")
    public PlayerGeneralResp previousOrNext(@RequestBody PreviousOrNextReq orNextReq) {
        return playerGeneralService.previousOrNext(orNextReq);
    }

    /**
     * 修改昵称
     *
     * @param id
     * @param nickName
     */
    @GetMapping("/updateGeneralName")
    public void updateGeneralName(Integer id, String nickName) {
        playerGeneralService.updatePlayerName(id, nickName);
    }

    /**
     * 放逐
     *
     * @param exileReq
     * @return
     */
    @PostMapping("/exile")
    public List<PlayerItem> exile(@RequestBody ExileReq exileReq) {
        return playerGeneralService.exile(exileReq);
    }

    /**
     * 合成
     *
     * @param playerGeneralSynthesisReq
     * @return
     */
    @PostMapping("/synthesis")
    public PlayerGeneral synthesis(@RequestBody PlayerGeneralSynthesisReq playerGeneralSynthesisReq) {
        return playerGeneralService.synthesis(playerGeneralSynthesisReq);
    }

    /**
     * 查询未驻扎的武将
     *
     * @return
     */
    @GetMapping("/listUnTerrain")
    public List<PlayerGeneral> listUnTerrain() {
        return playerGeneralService.listUnTerrain(AuthUtil.getCurrPlayerId());
    }


    /**
     * 查询未驻扎的武将和已驻扎的武将
     *
     * @return
     */
    @GetMapping("/listByTerrainAndUnTerrain")
    public TerrainAndUnTerrainResp listByTerrainAndUnTerrain(Integer territoryId) {
        return playerGeneralService.listByTerrainAndUnTerrain(territoryId);
    }


    /**
     * 驻军
     *
     * @param garrisonReq
     */
    @PostMapping("/garrison")
    public void garrison(@RequestBody GarrisonReq garrisonReq) {
        playerGeneralService.garrison(garrisonReq);
    }
    /**
     * 移除驻军
     *
     * @param garrisonReq
     */
    @PostMapping("/removeGarrison")
    public void removeGarrison(@RequestBody GarrisonReq garrisonReq) {
        playerGeneralService.removeGarrison(garrisonReq);
    }

}
