package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.ai.model.Position;
import com.example.sanguogameserver.dto.battle.*;
import com.example.sanguogameserver.logic.BattleLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 攻打任务 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-05-15
 */
@RestController
@RequestMapping("/api/battle")
public class BattleController {

    @Autowired
    private BattleLogic battleLogic;


    /**
     * 详情
     *
     * @param id
     * @return
     */
    @GetMapping("/getBattleDetail/{id}")
    public BattleDetail getBattleDetail(@PathVariable Integer id) {
        return battleLogic.getBattleDetail(id);
    }

    /**
     * 修改武将位置
     *
     * @param updatePlayerGeneralSortReq
     * @return
     */
    @PostMapping("/updatePlayerGeneralSort/{id}")
    public BattleDetail updatePlayerGeneralSort(@RequestBody UpdatePlayerGeneralSortReq updatePlayerGeneralSortReq, @PathVariable Integer id) {
        updatePlayerGeneralSortReq.setAttackTaskId(id);
        return battleLogic.updatePlayerGeneralSort(updatePlayerGeneralSortReq);
    }


    /**
     * 玩家走
     *
     * @param battleGoReq
     * @param id
     * @return
     */
    @PostMapping("/go/{id}")
    public BattleDetail go(@RequestBody BattleGoReq battleGoReq, @PathVariable Integer id) {
        battleGoReq.setAttackTaskId(id);
        return battleLogic.go(battleGoReq);
    }

    /**
     * 蓝方回合开始
     *
     * @param id
     * @return
     */
    @PostMapping("/startBlueRound/{id}")
    public BattleDetail startBlueRound(@PathVariable Integer id) {
        return battleLogic.startBlueRound(id);
    }

    /**
     * 红方回合开始
     *
     * @param id
     * @return
     */
    @PostMapping("/startRedRound/{id}")
    public BattleDetail startRedRound(@PathVariable Integer id) {
        return battleLogic.startRedRound(id);
    }

    /**
     * 开始,双方玩家都来了
     *
     * @param startBattleReq
     * @param id
     * @return
     */
    @PostMapping("/start/{id}")
    public BattleDetail start(@RequestBody StartBattleReq startBattleReq, @PathVariable Integer id) {
        startBattleReq.setAttackTaskId(id);
        return battleLogic.start(startBattleReq);
    }

    /**
     * 获取可以移动的范围
     *
     * @param canMovePositionReq
     * @param id
     * @return
     */
    @GetMapping("/canMovePosition/{id}")
    public List<Position> getCanMovePosition(GetCanMovePositionReq canMovePositionReq, @PathVariable Integer id) {
        canMovePositionReq.setId(id);
        return battleLogic.getCanMovePosition(canMovePositionReq);
    }

    /**
     * 获取可以攻击的范围
     *
     * @param getCanAttackPositionReq
     * @param id
     * @return
     */
    @GetMapping("/canAttackPosition/{id}")
    public List<Position> getCanAttackPosition(GetCanAttackPositionReq getCanAttackPositionReq, @PathVariable Integer id) {
        getCanAttackPositionReq.setId(id);
        return battleLogic.getCanAttackPosition(getCanAttackPositionReq);
    }

    /**
     * 获取可以技能攻击的范围
     *
     * @param getCanAttackPositionReq
     * @param id
     * @return
     */
    @GetMapping("/canSkillAttackPosition/{id}")
    public List<Position> getCanSkillAttackPosition(GetCanAttackPositionReq getCanAttackPositionReq, @PathVariable Integer id) {
        getCanAttackPositionReq.setId(id);
        return battleLogic.getCanSKillAttackPosition(getCanAttackPositionReq);
    }


}
