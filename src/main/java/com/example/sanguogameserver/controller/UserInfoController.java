package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.annotations.IgnoreAuth;
import com.example.sanguogameserver.dto.CurrentPlayer;
import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.entity.UserInfo;
import com.example.sanguogameserver.request.PlayerRegister;
import com.example.sanguogameserver.request.UserLogin;
import com.example.sanguogameserver.request.UserRegister;
import com.example.sanguogameserver.response.base.Result;
import com.example.sanguogameserver.service.UserInfoService;
import com.example.sanguogameserver.util.AuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
@RestController
@RequestMapping("/api/userInfo")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;

    @PostMapping("/register")
    @IgnoreAuth
    public UserInfo register(@RequestBody UserRegister userRegister) {
      return   userInfoService.register(userRegister);
    }

    @PostMapping("/login")
    @IgnoreAuth
    public UserInfo login(@RequestBody UserLogin userLogin) {
        return userInfoService.login(userLogin);
    }
}
