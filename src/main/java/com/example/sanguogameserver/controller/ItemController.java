package com.example.sanguogameserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 道具 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-18
 */
@RestController
@RequestMapping("/item")
public class ItemController {

}
