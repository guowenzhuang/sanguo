package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.response.PlayerItemResp;
import com.example.sanguogameserver.service.PlayerItemService;
import com.example.sanguogameserver.util.AuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 玩家道具 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-18
 */
@RestController
@RequestMapping("/api/playerItem")
public class PlayerItemController {
    @Autowired
    private PlayerItemService playerItemService;

    @GetMapping("/listALl")
    public List<PlayerItemResp> listAll() {
        return playerItemService.listAllByPlayerId(AuthUtil.getCurrPlayerId());
    }


}
