package com.example.sanguogameserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 群成员表 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/groupPlayer")
public class GroupPlayerController {

}
