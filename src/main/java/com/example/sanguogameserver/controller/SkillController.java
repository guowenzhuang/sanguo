package com.example.sanguogameserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 技能 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
@RestController
@RequestMapping("/api/skill")
public class SkillController {

}
