package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.request.AttackCreateTaskReq;
import com.example.sanguogameserver.response.AttackTaskResp;
import com.example.sanguogameserver.service.AttackTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 攻打任务 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-05-15
 */
@RestController
@RequestMapping("/api/attackTask")
public class AttackTaskController {

    @Autowired
    private AttackTaskService attackTaskService;

    /**
     * 攻打
     */
    @PostMapping("/attack")
    public void attack(@RequestBody AttackCreateTaskReq attackCreateTaskReq) {
        attackTaskService.attack(attackCreateTaskReq);
    }

    @PostMapping("/enterAttackTask/{id}")
    public void enterAttackTask(@PathVariable Integer id) {
        attackTaskService.enterAttackTask(id);
    }
    /**
     * 战斗记录/列表
     *
     * @return
     */
    @GetMapping("/list")
    public List<AttackTaskResp> list() {
        return attackTaskService.listRecord();
    }
}
