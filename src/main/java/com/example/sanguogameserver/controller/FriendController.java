package com.example.sanguogameserver.controller;

import com.example.sanguogameserver.request.AddFriendReq;
import com.example.sanguogameserver.request.ProcessingAddFriendListReq;
import com.example.sanguogameserver.response.AddFriendListResp;
import com.example.sanguogameserver.response.FriendGroupResp;
import com.example.sanguogameserver.response.FriendListResp;
import com.example.sanguogameserver.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-25
 */
@RestController
@RequestMapping("/api/friend")
public class FriendController {

    @Autowired
    private FriendService friendService;

    /**
     * 发起好友添加邀请
     */
    @PostMapping("/addFriend")
    public void addFriend(@RequestBody AddFriendReq addFriendReq) {
        friendService.addFriend(addFriendReq);
    }

    /**
     * 查看申请的好友
     *
     * @return
     */
    @PostMapping("/addFriendList")
    public List<AddFriendListResp> addFriendList() {
        return friendService.addFriendList();
    }

    /**
     * 处理添加好友请求
     */
    @PostMapping("/processingAddFriendList")
    public void processingAddFriendList(@RequestBody ProcessingAddFriendListReq processingAddFriendListReq) {
        friendService.processingAddFriendList(processingAddFriendListReq);
    }

    @GetMapping("/list")
    public List<FriendGroupResp> friendList(){
        return friendService.friendList();
    }
}
