package com.example.sanguogameserver.request;

import com.example.sanguogameserver.enums.HeadEnum;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdatePlayerInfo {



    private HeadEnum head;

    private String playName;

    private String countryName;

    private String flagColor;}
