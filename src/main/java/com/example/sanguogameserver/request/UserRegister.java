package com.example.sanguogameserver.request;

import lombok.Data;

@Data
public class UserRegister {
    /**
     * 用户名
     */
    private String account;
    /**
     * 密码
     */
    private String password;
}
