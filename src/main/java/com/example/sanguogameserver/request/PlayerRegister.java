package com.example.sanguogameserver.request;

import lombok.Data;

@Data
public class PlayerRegister {

    private String nickName;

    private String countryName;

    private String flagColor;

    private Integer userId;

    private Integer serverId;
}
