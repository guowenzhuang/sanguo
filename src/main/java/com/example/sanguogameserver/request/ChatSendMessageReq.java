package com.example.sanguogameserver.request;

import cn.hutool.core.date.DateUtil;
import com.example.sanguogameserver.enums.ChatTypeEnum;
import com.example.sanguogameserver.enums.HeadEnum;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class ChatSendMessageReq {
    /**
     * 玩家
     */
    private Integer playerId;

    private String groupId;

    private ChatTypeEnum chatType;

    /**
     * 发送内容
     */
    private String message;

    private LocalDateTime sendDate;

    // 世界聊天必传
    private Integer serverId;
}
