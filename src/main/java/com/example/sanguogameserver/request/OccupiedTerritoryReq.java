package com.example.sanguogameserver.request;

import lombok.Data;

import java.util.List;

@Data
public class OccupiedTerritoryReq {

    /**
     * 被攻击领地id
     */
    private Integer territoryId;

    private List<PlayerGeneralIdIndex> playerGenerals;
}
