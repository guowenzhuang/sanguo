package com.example.sanguogameserver.request;

import lombok.Data;

import java.util.List;

@Data
public class ExileReq {
    private List<Integer> ids;
}
