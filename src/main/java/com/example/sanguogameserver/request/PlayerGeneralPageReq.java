package com.example.sanguogameserver.request;

import com.example.sanguogameserver.request.base.BasePageReq;
import lombok.Data;

@Data
public class PlayerGeneralPageReq extends BasePageReq {
}
