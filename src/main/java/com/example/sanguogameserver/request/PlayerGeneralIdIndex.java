package com.example.sanguogameserver.request;

import lombok.Data;

@Data
public class PlayerGeneralIdIndex {
    private Integer id;
    private Integer index;
}
