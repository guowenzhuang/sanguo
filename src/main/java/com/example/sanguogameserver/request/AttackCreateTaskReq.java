package com.example.sanguogameserver.request;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class AttackCreateTaskReq {
    /**
     * 攻打时间
     */
    private LocalDateTime arrivalTime;

    /**
     * 被攻击领地id
     */
    private Integer beAttackTerritoryId;

    private List<PlayerGeneralIdIndex> playerGenerals;
}
