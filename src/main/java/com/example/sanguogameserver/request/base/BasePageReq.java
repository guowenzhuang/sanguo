package com.example.sanguogameserver.request.base;

import lombok.Data;

@Data
public class BasePageReq {
    private int page = 1;

    private int size = 10;
}
