package com.example.sanguogameserver.request;

import lombok.Data;
import org.springframework.data.relational.core.sql.In;

import java.util.List;

@Data
public class CreateGroupReq {
    private List<Integer> playerIds;
}
