package com.example.sanguogameserver.request;

import com.example.sanguogameserver.enums.FriendStatusEnum;
import lombok.Data;

@Data
public class ProcessingAddFriendListReq {
    private Integer friendId;
    /**
     * 好友状态
     */
    private FriendStatusEnum friendStatus;
}
