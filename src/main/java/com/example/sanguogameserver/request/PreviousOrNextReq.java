package com.example.sanguogameserver.request;

import cn.hutool.core.lang.Matcher;
import lombok.Data;
import org.springframework.data.relational.core.sql.In;

@Data
public class PreviousOrNextReq implements Matcher<Integer> {
    private Integer id;
    private boolean isNext;

    @Override
    public boolean match(Integer t) {
        return t.equals(id);
    }
}
