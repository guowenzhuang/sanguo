package com.example.sanguogameserver.request;

import lombok.Data;

import java.util.List;

@Data
public class PullPlayerGroupReq {

    private Integer groupId;

    private List<Integer> playerIds;
}
