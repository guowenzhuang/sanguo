package com.example.sanguogameserver.request;

import lombok.Data;

@Data
public class RaffleRequest {
    /**
     * 抽奖次数
     */
    private int count;
}
