package com.example.sanguogameserver.request;

import lombok.Data;

@Data
public class UserLogin {
    /**
     * 用户名
     */
    private String account;
    /**
     * 密码
     */
    private String password;
}
