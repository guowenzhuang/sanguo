package com.example.sanguogameserver.request;

import lombok.Data;

/**
 * 添加好友
 */
@Data
public class AddFriendReq {
    /**
     * 逐鹿id
     */
    private String zhuLuId;
}
