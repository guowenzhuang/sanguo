package com.example.sanguogameserver.request;

import lombok.Data;

import java.util.List;

@Data
public class GarrisonReq {

    private Integer playerGeneralId;

    private Integer terrainId;

    private boolean isCaptain;

    private int territoryIndex;
}
