package com.example.sanguogameserver.request;

import lombok.Data;

@Data
public class PlayerGeneralSynthesisReq {
    private Integer playerItemId;
}
