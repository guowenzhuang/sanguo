package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.Chat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-26
 */
public interface ChatMapper extends BaseMapper<Chat> {

    List<Chat> selectByReceivePlayerId(Integer playerId);
}
