package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.General;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
public interface GeneralMapper extends BaseMapper<General> {

}
