package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.Friend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sanguogameserver.response.AddFriendListResp;
import com.example.sanguogameserver.response.FriendGroupResp;
import com.example.sanguogameserver.response.FriendListResp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-25
 */
public interface FriendMapper extends BaseMapper<Friend> {

    List<AddFriendListResp> addFriendList(@Param("currPlayerId") Integer currPlayerId);

    /**
     * 查询好友列表
     * @param currPlayerId
     * @return
     */
    List<FriendGroupResp> friendList(Integer currPlayerId);

    Friend getByTwoPlayerId(@Param("mePlayerId") Integer mePlayerId, @Param("friendPlayerId") Integer friendPlayerId);

    List<Friend> getFriends(Integer playerId, List<Integer> friendPlayerId);
}
