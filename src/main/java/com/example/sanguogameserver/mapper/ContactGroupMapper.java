package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.ContactGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sanguogameserver.response.GroupListResp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 群组表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
public interface ContactGroupMapper extends BaseMapper<ContactGroup> {


    List<GroupListResp> listByPlayerId(@Param("playerId") Integer playerId);


    GroupListResp getById(@Param("id") Integer id);
}
