package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.SkillGeneralType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 武将类型技能表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
public interface SkillGeneralTypeMapper extends BaseMapper<SkillGeneralType> {

}
