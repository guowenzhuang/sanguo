package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.GeneralType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 武将类型 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
public interface GeneralTypeMapper extends BaseMapper<GeneralType> {

}
