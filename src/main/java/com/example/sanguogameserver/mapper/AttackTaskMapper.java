package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.AttackTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sanguogameserver.enums.BattleEnum;
import com.example.sanguogameserver.response.AttackTaskResp;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 攻打任务 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-05-15
 */
public interface AttackTaskMapper extends BaseMapper<AttackTask> {

    List<AttackTaskResp> listRecord(@Param("currPlayerId") Integer currPlayerId);

    List<AttackTask> selectTodoAttackTask(@Param("now") LocalDateTime now, @Param("battleEnum") BattleEnum battleEnum);
}
