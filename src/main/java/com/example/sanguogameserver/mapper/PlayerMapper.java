package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.Player;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sanguogameserver.response.MeInfoResp;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
public interface PlayerMapper extends BaseMapper<Player> {

}
