package com.example.sanguogameserver.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.sanguogameserver.dto.PlayerGeneralStartSumDTO;
import com.example.sanguogameserver.entity.PlayerGeneral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.sanguogameserver.request.PlayerGeneralPageReq;
import com.example.sanguogameserver.response.PlayerGeneralResp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 玩家武将 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-15
 */
public interface PlayerGeneralMapper extends BaseMapper<PlayerGeneral> {

    Page<PlayerGeneralResp> pageRecord(@Param("playerGeneralPageReq") PlayerGeneralPageReq playerGeneralPageReq, Page<PlayerGeneralResp> page);
    List<Integer> listAll(@Param("playerGeneralPageReq") PlayerGeneralPageReq playerGeneralPageReq);

    PlayerGeneralStartSumDTO getPlayerGeneralStartSumDTO(@Param("playerId") Integer playerId);

    List<PlayerGeneral> selectUnTerrain(Integer playerId);

    List<PlayerGeneral> selectByTerritoryId(@Param("territoryId") Integer territoryId, @Param("playerId") Integer playerId);
}
