package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.Server;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-05
 */
public interface ServerMapper extends BaseMapper<Server> {

}
