package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.SkillPlayerGeneral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 玩家武将技能表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
public interface SkillPlayerGeneralMapper extends BaseMapper<SkillPlayerGeneral> {

}
