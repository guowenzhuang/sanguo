package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.PlayerItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 玩家道具 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-18
 */
public interface PlayerItemMapper extends BaseMapper<PlayerItem> {

}
