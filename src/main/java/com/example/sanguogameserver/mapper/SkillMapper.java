package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.Skill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 技能 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-04-03
 */
public interface SkillMapper extends BaseMapper<Skill> {

    Integer randomSkillRow();

}
