package com.example.sanguogameserver.mapper;

import com.example.sanguogameserver.entity.GroupPlayer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 群成员表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-28
 */
public interface GroupPlayerMapper extends BaseMapper<GroupPlayer> {

}
