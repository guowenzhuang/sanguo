package com.example.sanguogameserver.response;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class GroupListResp {
    private Integer contactGroupId;

    /**
     * 群id
     */
    private String groupId;

    /**
     * 名称
     */
    private String groupName;

    /**
     * 群公告
     */
    private String groupNotice;

    /**
     * 创建时间
     */
    private LocalDateTime groupCreateTime;

    private List<GroupPlayerResp> players;

    private List<GroupPlayerResp> groupHeads;
}
