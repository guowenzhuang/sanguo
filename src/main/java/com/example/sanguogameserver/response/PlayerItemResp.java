package com.example.sanguogameserver.response;

import com.example.sanguogameserver.enums.ItemTypeEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PlayerItemResp {
    private Integer id;

    private Integer playerId;

    private Integer itemId;

    /**
     * 名称
     */
    private String itemName;

    /**
     * 介绍
     */
    private String itemDesc;

    /**
     * 类型(1:道具,2:经验书,3:武将碎片,4:回复hp 5: 回复mp)
     */
    private ItemTypeEnum itemType;

    /**
     * 数量
     */
    private Integer itemCount;

    /**
     * 获得时间
     */
    private LocalDateTime createDate;

    /**
     * 过期时间
     */
    private LocalDateTime expiredDate;

    /**
     * 量(当是回复hp和mp,时代表回复多少值)
     */
    private String quantity;

    /**
     * 武将id
     */
    private Integer generalId;
}
