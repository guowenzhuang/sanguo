package com.example.sanguogameserver.response;

import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.enums.HeadEnum;
import com.example.sanguogameserver.enums.OnlineEnum;
import lombok.Data;

@Data
public class FriendListResp {

    private Integer friendId;

    private HeadEnum head;

    private String playName;

    private Integer playerId;

    /**
     * 逐鹿id
     */
    private String zhuLuId;

    private String countryName;

    private OnlineEnum online;
}
