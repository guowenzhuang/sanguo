package com.example.sanguogameserver.response;

import com.example.sanguogameserver.entity.Server;
import com.example.sanguogameserver.entity.Territory;
import com.example.sanguogameserver.enums.HeadEnum;
import com.example.sanguogameserver.enums.OnlineEnum;
import com.example.sanguogameserver.enums.TitleEnum;
import lombok.Data;

import java.util.List;

@Data
public class MeInfoResp {
    private String zhuLuId;

    private HeadEnum head;

    private String playName;

    private String countryName;

    private String flagColor;

    private TitleEnum title;

    private Integer territoryNum;

    private Integer resourcesTotal;

    private Server server;

    private OnlineEnum online;

    private List<Territory> territories;

    private int playerGeneralStart1Sum;

    private int playerGeneralStart2Sum;

    private int playerGeneralStart3Sum;

    private int playerGeneralStart4Sum;

    private int playerGeneralStart5Sum;

    private int createDay;
}
