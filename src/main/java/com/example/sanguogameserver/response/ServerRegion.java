package com.example.sanguogameserver.response;

import com.example.sanguogameserver.entity.Server;
import lombok.Data;

import java.util.List;

@Data
public class ServerRegion {
    private String regionServer;
    private List<Server> servers;
}
