package com.example.sanguogameserver.response;

import lombok.Data;

@Data
public class RangePositionDetail {
    /**
     * 是否是玩家
     */
    private boolean isPlay;
    /**
     * 是否是高亮
     */
    private boolean isHighlight;
}
