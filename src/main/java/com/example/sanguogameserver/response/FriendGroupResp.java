package com.example.sanguogameserver.response;

import lombok.Data;

import java.util.List;

@Data
public class FriendGroupResp {


    private String group;

    private List<FriendListResp> friends;

}
