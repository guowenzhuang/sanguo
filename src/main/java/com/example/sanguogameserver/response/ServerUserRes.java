package com.example.sanguogameserver.response;

import com.example.sanguogameserver.entity.Server;
import lombok.Data;

import java.util.List;

@Data
public class ServerUserRes {
    private List<Server> userServer;

    public List<ServerRegion> serverRegions;
}
