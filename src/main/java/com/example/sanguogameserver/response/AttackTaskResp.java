package com.example.sanguogameserver.response;

import com.example.sanguogameserver.enums.BattleEnum;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AttackTaskResp {
    private Integer id;

    /**
     * 攻打时间
     */
    private LocalDateTime arrivalTime;

    /**
     * 领地id
     */
    private Integer territoryId;

    /**
     * 领地名称
     */
    private String territoryName;

    /**
     * 攻打玩家id
     */
    private Integer attackPlayerId;

    /**
     * `
     * 攻打玩家名称
     */
    private String attackPlayerName;

    /**
     * 攻击玩家所属阵容
     */
    private TreePlayerTypeEnum attackTreePlayerType;

    /**
     * 攻打玩家id
     */
    private Integer beAttackPlayerId;

    /**
     * 攻打玩家名称
     */
    private String beAttackPlayerName;

    /**
     * 攻击玩家所属阵容
     */
    private TreePlayerTypeEnum beAttackTreePlayerType;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    private BattleEnum isBattle;
}
