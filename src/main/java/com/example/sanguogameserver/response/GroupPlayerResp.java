package com.example.sanguogameserver.response;

import com.example.sanguogameserver.enums.DingTypeEnum;
import com.example.sanguogameserver.enums.GroupPlayerTypeEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GroupPlayerResp {
    private Integer groupPlayerId;

    /**
     * 群id
     */
    private String groupId;

    /**
     * 名称
     */
    private String groupName;

    /**
     * 玩家id
     */
    private Integer playerId;

    /**
     * 玩家昵称
     */
    private String playerName;

    /**
     * 玩家头像
     */
    private String playerHead;

    /**
     * 玩家类型(群主,管理员,普通)
     */
    private GroupPlayerTypeEnum groupPlayerType;

    /**
     * 消息提醒类型(正常,免打扰,特别提醒)
     */
    private DingTypeEnum dingType;

    /**
     * 创建时间
     */
    private LocalDateTime groupPlayerCreateTime;
}
