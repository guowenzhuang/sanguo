package com.example.sanguogameserver.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.sanguogameserver.entity.Player;
import com.example.sanguogameserver.entity.PlayerGeneral;
import lombok.Data;

@Data
public class TerritoryAllResp {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String territoryName;

    private Integer resourcesResidualTotal;

    private Integer resourcesSeconds;

    private Integer playId;

    private Float positionX;

    private Float positionY;

    private Player player;

    private Integer level;

    /**
     * 武将最大参战人数
     */
    private Integer generalMaxCount;

    private PlayerGeneral playerGeneral;
}
