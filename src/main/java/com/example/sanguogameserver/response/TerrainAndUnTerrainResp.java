package com.example.sanguogameserver.response;

import com.example.sanguogameserver.entity.PlayerGeneral;
import lombok.Data;

import java.util.List;

@Data
public class TerrainAndUnTerrainResp {
    /**
     * 未驻扎武将
     */
    private List<PlayerGeneral> unTerrainPlayer;
    /**
     * 已驻扎武将
     */
    private List<PlayerGeneral> terrainPlayer;
}
