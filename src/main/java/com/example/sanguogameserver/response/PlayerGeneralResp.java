package com.example.sanguogameserver.response;

import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Skill;
import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import com.example.sanguogameserver.enums.MutationTypeEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PlayerGeneralResp {
    private Integer id;

    private Integer playId;

    private Integer generalId;

    /**
     * 血量
     */
    private Integer health;

    /**
     * 法术蓝条
     */
    private Integer mp;

    /**
     * 武将类型
     */
    private GeneralCategoryEnum generalCategory;

    /**
     * 武将名称
     */
    private String generalName;

    /**
     * 攻击力
     */
    private Integer attack;

    /**
     * 防御
     */
    private Integer defense;

    /**
     * 法术强度
     */
    private Integer spellStrength;

    /**
     * 闪避率
     */
    private Integer dodge;

    /**
     * 暴击率
     */
    private Integer crit;

    /**
     * 血量增幅
     */
    private BigDecimal healthIncreaseRate;

    /**
     * 蓝量增幅
     */
    private BigDecimal mpIncreaseRate;

    /**
     * 攻击增幅
     */
    private BigDecimal attackIncreaseRate;

    /**
     * 防御增幅
     */
    private BigDecimal defenseIncreaseRate;

    /**
     * 法术强度增幅
     */
    private BigDecimal spellStrengthIncreaseRate;

    /**
     * 闪避率增幅
     */
    private BigDecimal dodgeIncreaseRate;

    /**
     * 暴击率增幅
     */
    private BigDecimal critIncreaseRate;

    /**
     * 星级
     */
    private String star;

    /**
     * 成长系数
     */
    private BigDecimal growthCoefficient;

    /**
     * 经验值
     */
    private Integer level;
    /**
     * 当前经验值
     */
    private Long currentExp;
    /**
     * 下一级经验值
     */
    private Long nextExp;

    /**
     * 武将类型
     */
    private String generalType;

    private MutationTypeEnum mutationType;

    private GeneralResp general;

    private GeneralTypeResp generalTypeResp;

    private List<BattleTreeInfoRow> moveAndAttackRange;

    private List<Skill> skills;


}
