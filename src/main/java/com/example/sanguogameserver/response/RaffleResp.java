package com.example.sanguogameserver.response;

import com.example.sanguogameserver.entity.PlayerGeneral;
import com.example.sanguogameserver.entity.PlayerItem;
import lombok.Data;

import java.util.List;

@Data
public class RaffleResp {

    private int maxStar;

    private List<PlayerGeneral> playerGenerals;

    private List<PlayerItem> playerItems;
}
