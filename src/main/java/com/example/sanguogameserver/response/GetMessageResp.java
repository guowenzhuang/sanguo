package com.example.sanguogameserver.response;

import com.example.sanguogameserver.dto.MessageDTO;
import com.example.sanguogameserver.dto.battle.BattleDetail;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GetMessageResp {
    private MessageDTO worldMessageDTO;

    private List<MessageResp> messageResps = new ArrayList<>();

    private BattleDetail battleDetail;
}
