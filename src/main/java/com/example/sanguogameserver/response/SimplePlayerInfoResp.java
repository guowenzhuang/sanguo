package com.example.sanguogameserver.response;

import com.example.sanguogameserver.enums.HeadEnum;
import lombok.Data;

@Data
public class SimplePlayerInfoResp {
    private HeadEnum head;

    private String playName;

    /**
     * 逐鹿id
     */
    private String zhuLuId;

    private String countryName;
}
