package com.example.sanguogameserver.response;

import com.example.sanguogameserver.dto.MessageDTO;
import lombok.Data;

@Data
public class MessageResp {
    /**
     * 好友id
     */
    private Integer friendPlayerId;
    /**
     * 消息
     */
    private MessageDTO messageDTO;

}
