package com.example.sanguogameserver;

import cn.hutool.core.util.StrUtil;
import com.example.sanguogameserver.ai.enums.AiType;
import com.example.sanguogameserver.ai.enums.TreeType;
import com.example.sanguogameserver.ai.logic.AiLogic;
import com.example.sanguogameserver.ai.model.AiTreeGeneral;
import com.example.sanguogameserver.ai.model.TreeGeneral;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.enums.GeneralCategoryEnum;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AiTest {


    @Test
    public void aiTest() {
        int row = 15;
        int column = 50;

        List<BattleTreeInfoRow> treeInfoRows = new ArrayList<>();
        for (int i = 0; i < row; i++) {
            BattleTreeInfoRow treeInfoRow = new BattleTreeInfoRow();
            treeInfoRow.setRow(i);
            List<BattleTreeInfoColumn> treeInfoColumns = new ArrayList<>();

            for (int j = 0; j < column; j++) {
                BattleTreeInfoColumn treeInfoColumn = new BattleTreeInfoColumn();
                treeInfoColumn.setTreeType(TreeType.grassland);
                treeInfoColumn.setIsMove(true);
                treeInfoColumn.setRow(i);
                treeInfoColumn.setColumn(j);
                treeInfoColumns.add(treeInfoColumn);
            }

            treeInfoRow.setTreeInfoColumns(treeInfoColumns);
            treeInfoRows.add(treeInfoRow);
        }

        // init 玩家
        initPlayer(treeInfoRows);

        // init Ai
        initAi(treeInfoRows);

        AiLogic aiLogic = new AiLogic(treeInfoRows, TreePlayerTypeEnum.red);
        aiLogic.aiGo();

        drawConsole(treeInfoRows);
    }

    private void initAi(List<BattleTreeInfoRow> treeInfoRows) {
        BattleTreeInfoColumn treeInfoColumnAi1 = treeInfoRows.get(0).getTreeInfoColumns().get(7);
        AiTreeGeneral treeAiGeneral1 = new AiTreeGeneral();
        treeAiGeneral1.setAiType(AiType.passiveAttack);
        treeAiGeneral1.setHealth(100);
        treeAiGeneral1.setCurrentHealth(100);
        treeAiGeneral1.setId(1);
        treeAiGeneral1.setGeneralName("ai1");
        treeAiGeneral1.setMoveCount(4);
        treeAiGeneral1.setRangeCount(1);
        treeAiGeneral1.setGeneralCategoryEnum(GeneralCategoryEnum.mage);
        treeAiGeneral1.setAi(true);
        treeInfoColumnAi1.setPlayerTreeGeneral(treeAiGeneral1);
        treeInfoColumnAi1.setAi(true);

        BattleTreeInfoColumn treeInfoColumnAi2 = treeInfoRows.get(1).getTreeInfoColumns().get(7);
        AiTreeGeneral treeAiGeneral2 = new AiTreeGeneral();
        treeAiGeneral2.setHealth(100);
        treeAiGeneral2.setCurrentHealth(100);
        treeAiGeneral2.setAiType(AiType.activeAttack);
        treeAiGeneral2.setId(2);
        treeAiGeneral2.setGeneralName("ai2");
        treeAiGeneral2.setMoveCount(3);
        treeAiGeneral2.setRangeCount(1);
        treeAiGeneral2.setGeneralCategoryEnum(GeneralCategoryEnum.mage);
        treeAiGeneral2.setAi(true);
        treeInfoColumnAi2.setPlayerTreeGeneral(treeAiGeneral2);
        treeInfoColumnAi2.setAi(true);

        BattleTreeInfoColumn treeInfoColumnAi3 = treeInfoRows.get(2).getTreeInfoColumns().get(7);
        AiTreeGeneral treeAiGeneral3 = new AiTreeGeneral();
        treeAiGeneral3.setHealth(100);
        treeAiGeneral3.setCurrentHealth(100);
        treeAiGeneral3.setAiType(AiType.stayWhereYouAre);
        treeAiGeneral3.setId(3);
        treeAiGeneral3.setGeneralName("ai3");
        treeAiGeneral3.setMoveCount(4);
        treeAiGeneral3.setRangeCount(1);
        treeAiGeneral3.setGeneralCategoryEnum(GeneralCategoryEnum.mage);

        treeAiGeneral3.setAi(true);
        treeInfoColumnAi3.setPlayerTreeGeneral(treeAiGeneral3);
        treeInfoColumnAi3.setAi(true);
    }

    private static void initPlayer(List<BattleTreeInfoRow> treeInfoRows) {
        BattleTreeInfoColumn treeInfoColumnPlayer1 = treeInfoRows.get(1).getTreeInfoColumns().get(3);
        TreeGeneral treePlayerGeneral1 = new TreeGeneral();
        treePlayerGeneral1.setId(1);
        treePlayerGeneral1.setHealth(100);
        treePlayerGeneral1.setCurrentHealth(100);
        treePlayerGeneral1.setGeneralName("play1");
        treePlayerGeneral1.setGeneralCategoryEnum(GeneralCategoryEnum.mage);
        treePlayerGeneral1.setMoveCount(4);
        treePlayerGeneral1.setRangeCount(1);
        treeInfoColumnPlayer1.setPlayerTreeGeneral(treePlayerGeneral1);

        BattleTreeInfoColumn treeInfoColumnPlayer2 = treeInfoRows.get(3).getTreeInfoColumns().get(3);
        TreeGeneral treePlayerGeneral2 = new TreeGeneral();
        treePlayerGeneral2.setHealth(100);
        treePlayerGeneral2.setCurrentHealth(100);
        treePlayerGeneral2.setId(2);
        treePlayerGeneral2.setGeneralCategoryEnum(GeneralCategoryEnum.mage);
        treePlayerGeneral2.setGeneralName("play2");
        treePlayerGeneral2.setMoveCount(4);
        treePlayerGeneral2.setRangeCount(1);
        treeInfoColumnPlayer2.setPlayerTreeGeneral(treePlayerGeneral2);

        BattleTreeInfoColumn treeInfoColumnPlayer3 = treeInfoRows.get(5).getTreeInfoColumns().get(3);
        TreeGeneral treePlayerGeneral3 = new TreeGeneral();
        treePlayerGeneral3.setHealth(100);
        treePlayerGeneral3.setCurrentHealth(100);
        treePlayerGeneral3.setId(3);
        treePlayerGeneral3.setGeneralCategoryEnum(GeneralCategoryEnum.mage);
        treePlayerGeneral3.setGeneralName("play3");
        treePlayerGeneral3.setMoveCount(4);
        treePlayerGeneral3.setRangeCount(1);
        treeInfoColumnPlayer3.setPlayerTreeGeneral(treePlayerGeneral3);
    }


    public void drawConsole(List<BattleTreeInfoRow> treeInfoRows) {
        for (BattleTreeInfoRow treeInfoRow : treeInfoRows) {
            for (BattleTreeInfoColumn treeInfoColumn : treeInfoRow.getTreeInfoColumns()) {
                if (StrUtil.isNotEmpty(treeInfoColumn.getTestPrintStr())) {
                    System.out.print(treeInfoColumn.getTestPrintStr());
                } else if (!treeInfoColumn.isAi() && treeInfoColumn.getPlayerTreeGeneral() != null) {
                    System.out.print("p");
                } else if (treeInfoColumn.isAi() && treeInfoColumn.getPlayerTreeGeneral() != null) {
                    System.out.print("A");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
