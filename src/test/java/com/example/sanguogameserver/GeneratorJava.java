package com.example.sanguogameserver;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.sql.Types;
import java.util.Collections;

public class GeneratorJava {
    public static String parentDir = "C:\\Users\\shijian\\IdeaProjects\\sanguoGameServer";

    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://rm-bp142hfckq61d8719po.mysql.rds.aliyuncs.com:3306/sanguo",
                        "root", "Qq123456")
                .globalConfig(builder -> {
                    builder.author("baomidou")
                            .disableOpenDir()
                            // 设置作者
                            .outputDir(parentDir + "\\src\\main\\java"); // 指定输出目录
                })
                .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                    int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                    if (typeCode == Types.SMALLINT) {
                        // 自定义类型转换
                        return DbColumnType.INTEGER;
                    }
                    return typeRegistry.getColumnType(metaInfo);

                }))
                .packageConfig(builder -> {
                    builder.parent("com.example.sanguogameserver") // 设置父包名
                            // .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, parentDir + "\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder
                            .addInclude("attack_task")
                            .disableSqlFilter()
                            .entityBuilder().enableLombok().enableFileOverride()
                            .controllerBuilder().enableFileOverride().enableRestStyle()
                            .mapperBuilder().enableFileOverride()
                            .serviceBuilder().enableFileOverride();
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .injectionConfig(builder -> {
                })
                .execute();
    }
}
