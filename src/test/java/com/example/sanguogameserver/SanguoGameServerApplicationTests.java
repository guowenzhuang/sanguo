/*
package com.example.sanguogameserver;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.example.sanguogameserver.entity.Territory;
import com.example.sanguogameserver.service.TerritoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class SanguoGameServerApplicationTests {

    @Autowired
    private TerritoryService territoryService;

    @Test
    void contextLoads() {
    }

    class Position {
        double positionX;
        double positionY;
    }


    @Test
    void territoryRandomPosition() {
        // 已经存在的位置
        List<Position> existencePosition = new ArrayList<>();

        // 所有城市
        List<Territory> territories = territoryService.list();
        final double maxPositionX = territories.size() * 1;
        final double maxPositionY = territories.size() * 1;
        // 最小距离
        final int minDistance = 5;

        for (Territory territory : territories) {
            double positionX = RandomUtil.randomDouble(-maxPositionX, maxPositionX);
            double positionY = RandomUtil.randomDouble(-maxPositionY, maxPositionY);


            while (true) {
                positionX = RandomUtil.randomDouble(-maxPositionX, maxPositionX);
                positionY = RandomUtil.randomDouble(-maxPositionY, maxPositionY);
                boolean a = true;
                for (Position item : existencePosition) {
                    double distance = Math.sqrt(Math.pow((item.positionX - positionX), 2) + Math.pow((item.positionY - positionY), 2));
                    System.out.println(StrUtil.format("当前:{} 位置:{},{} 与:{},{} 相差:{}",territory.getTerritoryName(), positionX, positionY,item.positionX,item.positionY,distance));

                    System.out.println();
                    if (distance < minDistance) {
                        a = false;
                    }

                }
                if (a) {
                    break;
                }

            }
            territory.setPositionX((float) positionX);
            territory.setPositionY((float) positionY);
            Position position = new Position();
            position.positionX = positionX;
            position.positionY = positionY;
            existencePosition.add(position);

            territoryService.updateById(territory);


        }

    }

}
*/
