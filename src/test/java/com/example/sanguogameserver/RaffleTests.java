package com.example.sanguogameserver;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.example.sanguogameserver.entity.Territory;
import com.example.sanguogameserver.service.PlayerGeneralService;
import com.example.sanguogameserver.service.TerritoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class RaffleTests {

    @Autowired
    private PlayerGeneralService playerGeneralService;
    BigDecimal minCoefficient = BigDecimal.valueOf(0.7);
    BigDecimal maxCoefficient = BigDecimal.valueOf(1.2);

    @Test
    void raffleTests() {
        int count = 100;
        int oneGeneral = 0;
        int twoGeneral = 0;
        int threeGeneral = 0;
        int fourGeneral = 0;
        int fiveGeneral = 0;

        int oneMutationGeneral = 0;
        int twoMutationGeneral = 0;
        int threeMutationGeneral = 0;
        int fourMutationGeneral = 0;
        int fiveMutationGeneral = 0;

        int suipian = 0;

        for (int i = 0; i < count; i++) {
            if (playerGeneralService.isGeneral()) {

                if (playerGeneralService.isMutation()) {
                    BigDecimal healthCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal mpCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal attackCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal defenseCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal spellStrengthCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal dodgeCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal critCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal sumCoefficient = healthCoefficient.add(mpCoefficient).add(attackCoefficient).add(defenseCoefficient).add(spellStrengthCoefficient).add(dodgeCoefficient).add(critCoefficient);
                    BigDecimal growthCoefficient = sumCoefficient.divide(BigDecimal.valueOf(7), 2, RoundingMode.HALF_UP);
                    Integer star = playerGeneralService.transGrowCoefficientToStar(growthCoefficient);
                    if (star == 1) {
                        oneMutationGeneral++;
                    }
                    if (star == 2) {
                        twoMutationGeneral++;
                    }
                    if (star == 3) {
                        threeMutationGeneral++;
                    }
                    if (star == 4) {
                        fourMutationGeneral++;
                    }
                    if (star == 5) {
                        fiveMutationGeneral++;
                    }
                } else {
                    BigDecimal healthCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal mpCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal attackCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal defenseCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal spellStrengthCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal dodgeCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal critCoefficient = RandomUtil.randomBigDecimal(minCoefficient, maxCoefficient);
                    BigDecimal sumCoefficient = healthCoefficient.add(mpCoefficient).add(attackCoefficient).add(defenseCoefficient).add(spellStrengthCoefficient).add(dodgeCoefficient).add(critCoefficient);
                    BigDecimal growthCoefficient = sumCoefficient.divide(BigDecimal.valueOf(7), 2, RoundingMode.HALF_UP);
                    Integer star = playerGeneralService.transGrowCoefficientToStar(growthCoefficient);
                    if (star == 1) {
                        oneGeneral++;
                    }
                    if (star == 2) {
                        twoGeneral++;
                    }
                    if (star == 3) {
                        threeGeneral++;
                    }
                    if (star == 4) {
                        fourGeneral++;
                    }
                    if (star == 5) {
                        fiveGeneral++;
                    }
                }


            } else {
                suipian++;
            }
        }


        System.out.println("共抽取" + count + "次");
        System.out.println("碎片" + suipian + "次");
        System.out.println("一星英雄" + oneGeneral + "次");
        System.out.println("二星英雄" + twoGeneral + "次");
        System.out.println("三星英雄" + threeGeneral + "次");
        System.out.println("四星英雄" + fourGeneral + "次");
        System.out.println("五星英雄" + fiveGeneral + "次");
        System.out.println("变异一星英雄" + oneMutationGeneral + "次");
        System.out.println("变异二星英雄" + twoMutationGeneral + "次");
        System.out.println("变异三星英雄" + threeMutationGeneral + "次");
        System.out.println("变异四星英雄" + fourMutationGeneral + "次");
        System.out.println("变异五星英雄" + fiveMutationGeneral + "次");
    }

}
