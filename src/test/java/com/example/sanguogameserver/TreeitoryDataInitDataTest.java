package com.example.sanguogameserver;

import com.alibaba.fastjson2.JSON;
import com.example.sanguogameserver.ai.enums.TreeType;
import com.example.sanguogameserver.dto.battle.BattleDetail;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoColumn;
import com.example.sanguogameserver.dto.battle.BattleTreeInfoRow;
import com.example.sanguogameserver.entity.Territory;
import com.example.sanguogameserver.service.TerritoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Slf4j
public class TreeitoryDataInitDataTest {

    @Autowired
    private TerritoryService territoryService;

    /**
     * 念洲
     */
    @Test
    public void nianzhou() {
        // 念洲
        Territory territory = territoryService.getById(15);
        BattleDetail battleDetail = territory.getTreeData();
        if (battleDetail != null) {
            log.info("已有数据,清空后,再初始化");
            return;
        }

        battleDetail = new BattleDetail();
        List<BattleTreeInfoRow> trees = new ArrayList<>();
        // 设置草地
        for (int i = 0; i < 18; i++) {
            BattleTreeInfoRow battleTreeInfoRow = new BattleTreeInfoRow();

            List<BattleTreeInfoColumn> battleTreeInfoColumns = new ArrayList<>();
            for (int j = 0; j < 28; j++) {
                BattleTreeInfoColumn battleTreeInfoColumn = new BattleTreeInfoColumn();
                battleTreeInfoColumn.setTreeType(TreeType.grassland);
                battleTreeInfoColumns.add(battleTreeInfoColumn);
            }
            battleTreeInfoRow.setTreeInfoColumns(battleTreeInfoColumns);
            battleTreeInfoRow.setRow(i);
            trees.add(battleTreeInfoRow);
        }
        setStone(trees,0,6);
        setStone(trees,0,7);
        setStone(trees,0,8);
        setStone(trees,0,9);
        setStone(trees,0,27);
        setStone(trees,1,6);
        setStone(trees,1,7);
        setStone(trees,1,8);
        setStone(trees,1,9);
        setStone(trees,1,27);
        setStone(trees,2,7);
        setStone(trees,2,8);
        setStone(trees,2,27);
        setStone(trees,3,27);
        setStone(trees,4,27);
        for (int i = 4; i < 18; i++) {
            setStone(trees,i,0);
        }
        for (int i = 10; i < 18; i++) {
            setStone(trees,i,27);
        }
        setStone(trees,4,9);
        setStone(trees,4,10);
        setStone(trees,4,11);
        setStone(trees,4,12);
        setStone(trees,5,9);
        setStone(trees,5,10);
        setStone(trees,5,11);
        setStone(trees,5,12);
        setStone(trees,6,9);
        setStone(trees,6,10);
        setStone(trees,6,11);
        setStone(trees,6,12);
        setStone(trees,9,9);
        setStone(trees,9,10);
        setStone(trees,9,11);
        setStone(trees,9,12);
        setStone(trees,10,9);
        setStone(trees,10,10);
        setStone(trees,10,11);
        setStone(trees,10,12);
        setStone(trees,11,9);
        setStone(trees,11,10);
        setStone(trees,11,11);
        setStone(trees,11,12);
        setStone(trees,12,9);
        setStone(trees,12,10);
        setStone(trees,12,11);
        setStone(trees,12,12);
        setStone(trees,4,16);
        setStone(trees,5,16);
        setStone(trees,6,16);
        setStone(trees,8,16);
        setStone(trees,9,16);
        setStone(trees,10,16);
        setStone(trees,11,18);
        setStone(trees,12,18);
        setStone(trees,13,18);
        setStone(trees,14,18);
        setStone(trees,14,16);
        setStone(trees,14,17);
        setStone(trees,15,16);
        setStone(trees,16,16);
        setStone(trees,15,12);
        setStone(trees,15,13);
        setStone(trees,16,9);
        setStone(trees,16,10);
        setStone(trees,16,12);
        setStone(trees,16,16);

        // 设置障碍
        battleDetail.setTrees(trees);

        System.out.println(JSON.toJSONString(battleDetail));
    }

    private void setStone( List<BattleTreeInfoRow> trees,int row,int column){
        BattleTreeInfoColumn battleTreeInfoColumn = trees.get(row).getTreeInfoColumns().get(column);
        battleTreeInfoColumn.setTreeType(TreeType.stone);

    }
}
