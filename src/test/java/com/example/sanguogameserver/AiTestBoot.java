package com.example.sanguogameserver;

import com.alibaba.fastjson2.JSON;
import com.example.sanguogameserver.ai.logic.AiLogic;
import com.example.sanguogameserver.dto.battle.BattleDetail;
import com.example.sanguogameserver.dto.battle.BattleRoundInfo;
import com.example.sanguogameserver.enums.TreePlayerTypeEnum;
import com.example.sanguogameserver.logic.BattleLogic;
import com.example.sanguogameserver.service.AttackTaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AiTestBoot {

    @Autowired
    private AttackTaskService attackTaskService;

    @Autowired
    private BattleLogic battleLogic;

    @Test
    public void aiTest() {
        BattleDetail battleDetail = battleLogic.getBattleDetail(1);
        AiLogic aiLogic = new AiLogic(battleDetail.trees, TreePlayerTypeEnum.blue);
        BattleRoundInfo battleRoundInfo = aiLogic.aiGo();
        System.out.println(JSON.toJSONString(battleRoundInfo));
        System.out.println(JSON.toJSONString(battleDetail));
    }
}
